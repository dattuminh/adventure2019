#include "CharacterController.h"

namespace adventure::controller {

    CharacterController::CharacterController(std::vector<NPC> npc,
                                             std::weak_ptr<InventoryController> invC) {
        npcs.reserve(npc.size());
        inventoryCtrl = std::move(invC);
        for (auto &n : npc) {
            npcs.emplace(n.getId(), std::move(n));
        }
    }

    void CharacterController::setCharacterLocation(CharId userID, RoomId roomID) {
        getMutableCharacter(userID).setLocation(roomID);
    }

    RoomId CharacterController::getCharacterLocation(CharId id) const {
        return getCharacter(id).getLocation();
    }

    const Character &CharacterController::getCharacter(CharId characterID) const {
        for (const auto &character : characters) {
            if (character.getId() == characterID) {
                return character;
            }
        }
        LOG(FATAL) << "getCharacter called for non existant ID: " << characterID;
        throw std::runtime_error("getCharacter called for non existant ID: ");
    }

    Character &CharacterController::getMutableCharacter(CharId characterID) {
        for (auto &character : characters) {
            if (character.getId() == characterID) {
                return character;
            }
        }
        LOG(FATAL) << "getCharacter called for non existant ID: " << characterID;
        throw std::runtime_error("getCharacter called for non existant ID: ");
    }

    CharId CharacterController::addCharacter(std::string name) {
        auto inventoryId = inventoryCtrl.lock()->createInventory();
        return characters.emplace_back(Character{std::move(name), getNewId(), inventoryId}).getId();
    }

    std::string CharacterController::getCharacterName(CharId id) const {
        return getCharacter(id).getName();
    }

    std::vector<std::pair<CharId, std::string>> CharacterController::getKeywords(const std::vector<CharId> &ids) const {
        std::vector<std::pair<CharId, std::string>> ret;
        ret.reserve(ids.size() * 2); // Assume name + 1 keyword minimum

        for (const auto &id : ids) {
            auto character = getCharacter(id);
            const auto &keywords = character.getKeywords();
            for (const auto &k : keywords) {
                ret.emplace_back(id, k);
            }
            ret.emplace_back(id, character.getName());
        }

        return ret;
    }

    InventoryId CharacterController::getCharacterInventoryId(CharId id) const{
        auto character = getCharacter(id);
        return character.getInventory();
    }

    CharId CharacterController::spawnNPC(NPCId id, RoomId roomId) {
        auto npc = getNPC(id);
        auto charId = characters.emplace_back(npc, getNewId(), roomId).getId();
        return charId;
    }
}
