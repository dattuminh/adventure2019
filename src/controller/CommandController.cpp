#include "CommandController.h"


namespace adventure::controller {
    GameDataWrapper CommandController::generateResponse(const std::string &from, const std::string &identifier,
                                                        const std::string &response) {
        ChatMessage msg{from, response};
        json j = msg;
        return {identifier, j};
    }

    /*
     * Convert the JSON data into the correct Struct based on the type of Action.
     */
    template<typename StructModel>
    CCommand<StructModel> CommandController::toCommand(const GameDataWrapper &g) {
        CCommand<StructModel> command;
        command.id = playerController.lock()->getActiveAvatar(g.connection_identifier);
        command.data = g.data.get<StructModel>();
        return command;
    }

    /*
     * Inspect the incoming request for the type of Action, then generates the correct StructModel and passes it onto
     * the responsible controller.
     */
    void CommandController::handleCommand(const GameDataWrapper &g) {
        Action a = model::getAction(g.data);

        switch (a) {
            case Action::ChatCommand: {
                commController->handleCommCommand(toCommand<model::ChatCommand>(g));
                break;
            }
            case Action::ChatMessage: {
                auto c = toCommand<ChatMessage>(g);
                commController->handleChat(c);
                break;
            }
            case Action::Move: {
                moveController->handleMoveCommand(toCommand<model::Move>(g));
                break;
            }
            case Action::Inventory: {
                invController->handleInventoryCommand(toCommand<model::InventoryCommand>(g));
                break;
            }
            case Action::Join: {
                auto player_id = playerController.lock()->addPlayer(g.connection_identifier);
                moveController->spawnCharacter(player_id);
                break;
            }
            case Action::Look: {
                LOG(ERROR) << "Look not yet implemented";
                break;
            }
            default:
            case Action::Heartbeat:
            case Action::Status:
            case Action::Authenticate:
            case Action::Debug:
            case Action::Shutdown:
            case Action::Register:
            case Action::Model:
            case Action::Nil: {
                LOG(ERROR) << "Invalid Action for this Controller :" << a << " Request: " << g;
                outgoing.lock()->emplace_back(
                        generateResponse("Server", g.connection_identifier, "Invalid action for this Controller"));
            }
        }
    }
}
