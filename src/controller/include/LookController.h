#ifndef ADVENTURE2019_LOOKCONTROLLER_H
#define ADVENTURE2019_LOOKCONTROLLER_H

#include "StructModels.h"
#include "RoomController.h"
#include "CharacterController.h"
#include "PlayerController.h"
#include "Parser.h"

#include <string>
#include <nlohmann/json.hpp>
#include <memory>
#include <utility>

using adventure::model::Room;
using adventure::model::Command;
using adventure::model::GameDataWrapper;
using json = nlohmann::json;
using adventure::model::to_json;
using adventure::model::ChatMessage;

namespace adventure::controller {
    class LookController {
    public:
        LookController(std::shared_ptr<RoomController> roomC,
                       std::shared_ptr<PlayerController> userC,
                       std::weak_ptr<CharacterController> charC,
                       ResponseDataList out) : charController{charC} {
            roomController = roomC;
            playerController = userC;
            outgoing = out;
        }

        ~LookController() = default;

        void handleLookCommand(Command command);

    private:
        std::shared_ptr<RoomController> roomController;
        std::shared_ptr<PlayerController> playerController;
        std::weak_ptr<CharacterController> charController;

        ResponseDataList outgoing;
    };
}


#endif //ADVENTURE2019_LOOKCONTROLLER_H
