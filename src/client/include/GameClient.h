#ifndef ADVENTURE2019_GAMECLIENT_H
#define ADVENTURE2019_GAMECLIENT_H

#include "ChatWindow.h"
#include "AuthenticationWindow.h"
#include "CommandController.h"
#include "CommunicationController.h"
#include "Parser.h"
#include "StructModels.h"
#include "Adventure.h"
#include "ValidateInput.h"

#include <Client.h>
#include <utility>
#include <string>
#include <sstream>
#include <iostream>

using namespace adventure::view;
using adventure::model::ModelData;
using adventure::model::getAction;
using adventure::model::ServerStatus;
using adventure::model::AuthResponse;
using adventure::model::ErrorMessage;
using adventure::model::NPC;
using adventure::model::Area;
using adventure::model::Door;
using adventure::model::Extra;
using adventure::model::Object;
using adventure::model::Character;

namespace adventure::client {
    enum WindowType {CHAT, LOGIN};

    class GameClient {
    public:
        GameClient(const char *address, const char *port);
        void runGame();
        void callWindow(WindowType type);

    private:
        networking::Client client;
        ValidateInput inputChecker;

        Window *currentWindow = nullptr;
        std::map<WindowType, std::unique_ptr<Window>> windows;

        void onChatEntry(std::string text);
        void onAuthEntry(std::string username, std::string password);

        void clientReceiveCommand(std::string& commandStr);

        void processResponse(std::string &message);
        void processAuthMessage(const AuthResponse &response);
        void processServerStat(const ServerStatus &stat);
        void processModelData(const ModelData &model);
        void processRoomModel(const Room &room);
        void processDoorModel(const Door &door);
        void processAreaModel(const Area &area);
        void processNpcModel(const NPC &npc);
        void processObjectModel(const Object &object);
        void processExtraModel(const Extra &extra);
        void processConfirm(ModelType type, json j);

        inline bool connected(){return !client.isDisconnected();}

        void exitClient();
    };
}
#endif //ADVENTURE2019_GAMECLIENT_H
