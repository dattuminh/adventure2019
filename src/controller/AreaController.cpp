#include "AreaController.h"

namespace adventure::controller {

    void AreaController::receiveIncoming(GameDataWrapper data) {
        comHandler->handleCommand(data);
    }

    void AreaController::performAreaTick(std::chrono::seconds time_delta) {
        // Probably the only controller so far that needs to do something on a tick is Reset
        resetController->onTick(time_delta);
    }

    AreaController::AreaController(model::Area a) : model{a}, default_room{a.getRooms().front().getId()} {
        responseMap = std::make_shared<std::unordered_map<CharId, std::list<json>, StrongAliasIdHasher>>();
        outgoing = std::make_shared<std::list<model::GameDataWrapper>>();

        inventoryController = std::make_shared<InventoryController>(a.getObjects());

        characterController = std::make_shared<CharacterController>(a.getNpcs(), inventoryController);

        playerController = std::make_shared<PlayerController>(characterController, outgoing);

        roomController = std::make_shared<RoomController>(a.getRooms(), characterController, inventoryController, responseMap);

        lookController = std::make_shared<LookController>(roomController, playerController, characterController,
                                                          outgoing);

        moveController = std::make_shared<MovementController>(roomController, characterController, default_room,
                                                              responseMap);

        commController = std::make_shared<CommunicationController>(characterController, roomController, responseMap);

        invCommandController = std::make_shared<InvCommandController>(roomController, characterController, inventoryController, responseMap);

        comHandler = std::make_shared<CommandController>(lookController,
                                                         commController,
                                                         moveController,
                                                         playerController,
                                                         invCommandController,
                                                         outgoing);

        resetController = std::make_unique<ResetController>(model.getResets(), roomController, characterController,
                                                            inventoryController);
    }

    /**
     * Sends the outgoing messages for Characters that are Player controlled. Discards the rest.
     * NOTE: This is purposely inefficient so developers can assume all characters are Players
     * @return Reference to outgoing. It is assumed that the caller will call splice or clear to empty the list.
     */
    std::list<model::GameDataWrapper> &AreaController::getOutgoing() {
        for (auto &[id, messages] : *responseMap) {
            if (playerController->characterIsAvatar(id)) {
                auto identifier = playerController->avatarToPlayerName(id);
                auto out = this->outgoing;
                messages.remove_if(
                        [&out, &identifier](const auto &m) {
                            out->emplace_back(GameDataWrapper{.connection_identifier = identifier, .data = m});
                            return true; // Always return true as we want the message to be removed.
                        });
            } else {
                messages.clear(); // NPCs don't need to receive anything.
            }
        }
        return *outgoing;
    }
}
