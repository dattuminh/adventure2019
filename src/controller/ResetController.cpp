#include "ResetController.h"

using namespace adventure::controller;

ResetController::ResetController(std::vector<std::vector<Reset>> r, std::weak_ptr<RoomController> roomC,
                                 std::weak_ptr<CharacterController> charC, std::weak_ptr<InventoryController> invC)
        : reset_groups{std::move(r)},
          room_controller{std::move(roomC)},
          char_controller{std::move(charC)},
          inventory_controller{std::move(invC)} {
    checkResets();
}

void ResetController::onTick(std::chrono::seconds delta) {
    // Do resets when the elapsed time is greater than or equal to the configurable reset period
    if ((time_elapsed + delta) >= reset_period) {
        for (const auto &r : reset_groups) {
            performReset(r);
        }
        time_elapsed = std::chrono::seconds::zero(); // Set the elapsed time to 0 after doing the resets.
    } else {
        time_elapsed += delta;
    }
}

void ResetController::performReset(const std::vector<Reset> &resets) const {
    std::vector<CharId> npc;
    ItemId object_id;
    for (const auto &r : resets) {
        switch (r.getAction()) {
            case model::ResetAction::NPC: {
                npc = npcHandler(r);
                break;
            }
            case model::ResetAction::Give: {
                giveHandler(r, npc);
                break;
            }
            case model::ResetAction::Equip: {
                equipHandler(r, npc);
                break;
            }
            case model::ResetAction::Object: {
                object_id = objectHandler(r);
                break;
            }
            case model::ResetAction::Door: {
                doorHandler(r);
                break;
            }
            case model::ResetAction::Put: {
                putHandler(r, object_id);
                break;
            }
            default:
            case model::ResetAction::Nil:{
                LOG(ERROR) << "Should never see this. ResetAction:" << r;
            }
                break;
        }
    }
}

/*
 * It seems that the ID field for door resets are the index (counted from 0) of the doors for that room.
 */
void ResetController::doorHandler(const Reset &reset) const {
    auto room = room_controller.lock()->getRoom(*reset.getRoom());
    if (room.hasDoorIndex(reset.getId())) {
        room.getMutableDoorByIndex(reset.getId()).setState(*reset.getState());
    } else {
        LOG(ERROR) << "Invalid index for room " << room.getName() << " available doors are: ";
        for (const auto &d : room.getDoors()) {
            LOG(ERROR) << d;
        }
        LOG(ERROR) << " from " << reset;
    }
}

std::vector<CharId> ResetController::npcHandler(const Reset &reset) const {
    auto char_ctrl = char_controller.lock();
    auto npc_id = NPCId{reset.getId().id};
    auto room_id = *reset.getRoom();
    auto npc = char_ctrl->getNPC(npc_id);
    auto num_npcs = npc.getCharacters(room_id).size();

    if (num_npcs >= *reset.getLimit()) {
        return {};
    }

    auto room = room_controller.lock()->getRoom(room_id);
    std::vector<CharId> spawned_npc{};
    spawned_npc.reserve(*reset.getLimit());

    for (auto i = num_npcs; i < *reset.getLimit(); i++) {
        spawned_npc.emplace_back(char_ctrl->spawnNPC(npc_id, room_id));
    }

    return spawned_npc;
}

void ResetController::giveHandler(const Reset &reset, const std::vector<CharId> &characters) const {
    auto char_ctrl = char_controller.lock();
    auto inv_ctrl = inventory_controller.lock();
    ObjectType object{reset.getId()};
    for (const auto &c : characters) {
        inv_ctrl->addNewItem(object, char_ctrl->getCharacter(c).getInventory());
    }
}

void ResetController::equipHandler(const Reset &reset, const std::vector<CharId> &characters) const {
    // TODO: Implement this
    LOG(ERROR) << "Not implemented equipHandler()";
}

ItemId ResetController::objectHandler(const Reset &reset) const {
    auto inv_ctrl = inventory_controller.lock();
    auto room_inv_id = room_controller.lock()->getRoom(*reset.getRoom()).getInventory();
    ObjectType id{reset.getId()};
    if (inv_ctrl->isObjectInInventory(id, room_inv_id)) {
        // need to return ItemId for existing since it may still need to be reset
        const auto &items = inv_ctrl->getItemsInInventory(room_inv_id);
        auto itr = std::find_if(items.begin(), items.end(), [&id](const auto &item) {
            return item.getType() == id;
        });
        return itr->getId();
    } else {
        return inv_ctrl->addNewItem(id, room_inv_id);
    }
}


void ResetController::putHandler(const Reset &reset, ItemId object) const {
    auto inv_ctrl = inventory_controller.lock();
    ObjectType type{reset.getId()};
    //assert(inv_ctrl->getItem(object).getType() == type); Un comment when getItem() is implemented
    inv_ctrl->addNewItemToContainer(type, object);
}

void ResetController::checkResets() {
    for (auto &reset : reset_groups) {
        std::vector<Reset> keep{};
        keep.reserve(reset.size());

        for (const auto &r : reset) {
            switch (r.getAction()) {
                case adventure::model::ResetAction::Door: {
                    auto room = room_controller.lock()->getRoom(*r.getRoom());
                    if (room.hasDoorIndex(r.getId())) {
                        keep.emplace_back(r);
                    } else {
                        LOG(WARNING) << "Removing invalid " << r;
                    }
                    break;
                }
                case adventure::model::ResetAction::NPC:
                case adventure::model::ResetAction::Give:
                case adventure::model::ResetAction::Equip:
                case adventure::model::ResetAction::Object:
                case adventure::model::ResetAction::Put:keep.emplace_back(r);
                    break;
                default:
                case adventure::model::ResetAction::Nil: {
                    LOG(WARNING) << "Removing invalid Reset: " << r;
                    break;
                }
            }
        }

        reset.shrink_to_fit();
        reset.swap(keep);
    }
}
