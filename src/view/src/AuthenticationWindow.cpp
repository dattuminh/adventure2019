#include "AuthenticationWindow.h"

#include <cassert>

#include <form.h>
#include <ncurses.h>

#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>

namespace adventure::view {

    ////////////////////////////////////////////////////////////////////////////////
    // Hidden AuthWindow implementation                                           //
    ////////////////////////////////////////////////////////////////////////////////

    const int ENTRY_WIDTH = 24;
    const int ENTRY_HEIGHT = 12;
    const int FIELD_WIDTH = 22;
    const int FIELD_OFFSET_X = (ENTRY_WIDTH - FIELD_WIDTH) / 2;
    const int USERNAME_FIELD_OFFSET_Y = 2;
    const int PASSWORD_FIELD_OFFSET_Y = USERNAME_FIELD_OFFSET_Y + 4;
    const int OUTPUT_FIELD_OFFSET_Y = PASSWORD_FIELD_OFFSET_Y + 2;

    const char* USERNAME_LABEL = "Username:";
    const char* PASSWORD_LABEL = "Password:";

    class AuthWindowImpl {
    public:
        AuthWindowImpl(std::function<void(std::string, std::string)> onTextEntry, int updateDelay);
        ~AuthWindowImpl();
        AuthWindowImpl(AuthWindowImpl&) = delete;
        AuthWindowImpl(AuthWindowImpl&&) = delete;
        AuthWindowImpl& operator=(AuthWindowImpl&) = delete;
        AuthWindowImpl& operator=(AuthWindowImpl&&) = delete;

        void resizeOnShapeChange();

        void processInput(int key);

        FIELD* newLabelField(const char* label, int topRow);

        FIELD* newInputField(int topRow);

        void addFields();

        std::string getPasswordFieldString() const;

        std::string getUsernameFieldString() const;

        void updatePasswordField(int key);

        void refreshWindow();

        void redrawWindow();

        void displayText(const std::string& text);

    private:
        std::function<void(std::string, std::string)> onTextEntry;

        int parentX   = 0;
        int parentY   = 0;
        int entryY = 0;
        int entryX = 0;

        WINDOW *entry = nullptr;
        WINDOW *entrySub = nullptr;

        FIELD *outputLabel = nullptr;
        FIELD *usernameLabel = nullptr;
        FIELD *usernameField = nullptr;
        FIELD *passwordLabel = nullptr;
        FIELD *passwordField = nullptr;

        FORM *entryForm = nullptr;

        std::vector<FIELD*> fields;

        std::string password;
    };


    AuthWindowImpl::AuthWindowImpl(std::function<void(std::string, std::string)> onTextEntry,
                                   int updateDelay)
            : onTextEntry{std::move(onTextEntry)} {
        initscr();
        keypad(stdscr, TRUE);
        noecho();
        halfdelay(updateDelay);

        getmaxyx(stdscr, parentY, parentX);

        entryX = (parentX - ENTRY_WIDTH) / 2;
        entryY = (parentY - ENTRY_HEIGHT) / 2;

        entry = newwin(ENTRY_HEIGHT, ENTRY_WIDTH, entryY, entryX);

        entrySub = derwin(entry, ENTRY_HEIGHT - 2, ENTRY_WIDTH - 2, entryY + 2, entryX + 2);

        addFields();

        entryForm = new_form(fields.data());
        assert(entryForm && "Error creating entry form,");
        set_form_win(entryForm, entry);
        set_form_sub(entryForm, entrySub);
        post_form(entryForm);

        box(entry, 0, 0);

        refresh();
        wrefresh(entrySub);
    }


    AuthWindowImpl::~AuthWindowImpl() {
        unpost_form(entryForm);
        free_form(entryForm);
        free_field(outputLabel);
        free_field(usernameLabel);
        free_field(usernameField);
        free_field(passwordField);
        free_field(passwordLabel);
        delwin(entrySub);
        delwin(entry);
        endwin();
    }


    FIELD* AuthWindowImpl::newLabelField(const char* label, int topRow) {
        FIELD *labelField = new_field(2, FIELD_WIDTH, topRow, 1, 0, 0);
        assert(labelField && "Error creating label field");
        set_field_buffer(labelField, 0, label);
        set_field_opts(labelField, O_VISIBLE | O_PUBLIC | O_STATIC);

        return labelField;
    }


    FIELD* AuthWindowImpl::newInputField(int topRow) {
        FIELD* inputField = new_field(1, FIELD_WIDTH, topRow, FIELD_OFFSET_X, 0, 0);
        assert(inputField && "Error creating input field");
        set_field_buffer(inputField, 0, "");
        set_field_back(inputField, A_UNDERLINE);
        set_field_opts(inputField, O_VISIBLE | O_PUBLIC | O_STATIC | O_EDIT | O_ACTIVE);

        return inputField;
    }


    void AuthWindowImpl::addFields() {
        usernameLabel = newLabelField(USERNAME_LABEL, 1);
        usernameField = newInputField(USERNAME_FIELD_OFFSET_Y);

        passwordLabel = newLabelField(PASSWORD_LABEL, PASSWORD_FIELD_OFFSET_Y - 1);
        passwordField = newInputField(PASSWORD_FIELD_OFFSET_Y);

        outputLabel = newLabelField("", OUTPUT_FIELD_OFFSET_Y);

        fields.push_back(usernameLabel);
        fields.push_back(usernameField);
        fields.push_back(passwordLabel);
        fields.push_back(passwordField);
        fields.push_back(outputLabel);
        fields.push_back(nullptr);
    }


    void
    AuthWindowImpl::updatePasswordField(int key) {
        if(key == KEY_BACKSPACE || key == 127) {
            password.pop_back();
        } else {
            auto passwordFieldString = getPasswordFieldString();
            if(password.length() == passwordFieldString.length() - 1) {
                password.push_back(getPasswordFieldString().back());
            }
        }

        set_field_buffer(passwordField, 0, std::string(password.length(), '*').c_str());
    }


    void
    AuthWindowImpl::resizeOnShapeChange() {
        int newX, newY;
        getmaxyx(stdscr, newY, newX);

        if (newY != parentY || newX != parentX) {
            parentX = newX;
            parentY = newY;

            entryX = (parentX - ENTRY_WIDTH) / 2;
            entryY = (parentY - ENTRY_HEIGHT) / 2;

            mvwin(entry, entryY, entryX);

            wclear(stdscr);
        }
    }


    void
    AuthWindowImpl::processInput(int key) {
        switch(key) {
            case KEY_UP:
                form_driver(entryForm, REQ_PREV_FIELD);
                form_driver(entryForm, REQ_END_LINE);
                break;
            case KEY_DOWN:
                form_driver(entryForm, REQ_NEXT_FIELD);
                form_driver(entryForm, REQ_END_LINE);
                break;
            case KEY_ENTER:
            case '\n':
                form_driver(entryForm, REQ_VALIDATION);
                onTextEntry(getUsernameFieldString(), password);
                refresh();
                pos_form_cursor(entryForm);
                break;
            case KEY_BACKSPACE:
            case 127:
                form_driver(entryForm, REQ_DEL_PREV);
                form_driver(entryForm, REQ_VALIDATION);
                if(current_field(entryForm) == passwordField && !password.empty()) {
                    updatePasswordField(key);
                }
                form_driver(entryForm, REQ_END_FIELD);
                break;
            case KEY_DC:
                form_driver(entryForm, REQ_DEL_CHAR);
                break;
            case ERR:
                // swallow
                break;
            default:
                form_driver(entryForm, key);
                form_driver(entryForm, REQ_VALIDATION);
                if(current_field(entryForm) == passwordField) {
                    updatePasswordField(key);
                }
                form_driver(entryForm, REQ_END_FIELD);
                break;
        }
    }


    void
    AuthWindowImpl::refreshWindow() {
        wrefresh(entry);
        wrefresh(entrySub);
    }


    void
    AuthWindowImpl::redrawWindow() {
        clear();
        refresh();
    }


    void
    AuthWindowImpl::displayText(const std::string& text) {
        set_field_buffer(usernameField, 0, "");
        set_field_buffer(passwordField, 0, "");
        set_field_buffer(outputLabel, 0, text.c_str());

        password = "";
    }


    std::string
    AuthWindowImpl::getPasswordFieldString() const {
        auto passwordFieldString = std::string{field_buffer(passwordField, 0), FIELD_WIDTH};
        boost::trim(passwordFieldString);
        return passwordFieldString;
    }


    std::string
    AuthWindowImpl::getUsernameFieldString() const {
        auto usernameFieldString = std::string{field_buffer(usernameField, 0), FIELD_WIDTH};
        boost::trim(usernameFieldString);
        return usernameFieldString;
    }


    ////////////////////////////////////////////////////////////////////////////////
    // AuthenticationWindow API                                                   //
    ////////////////////////////////////////////////////////////////////////////////

    AuthenticationWindow::AuthenticationWindow(std::function<void(std::string, std::string)> onTextEntry, int updateDelay)
            : impl{std::make_unique<AuthWindowImpl>(std::move(onTextEntry), updateDelay)} {}

    AuthenticationWindow::~AuthenticationWindow() = default;


    void
    AuthenticationWindow::update() {
        impl->resizeOnShapeChange();
        impl->processInput(getch());
        impl->refreshWindow();
    }


    void
    AuthenticationWindow::redraw() {
        impl->redrawWindow();
        impl->refreshWindow();
    }


    void
    AuthenticationWindow::displayText(const std::string &text) {
        impl->displayText(text);
    }
}
