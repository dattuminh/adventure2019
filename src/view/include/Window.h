#ifndef ADVENTURE2019_WINDOW_H
#define ADVENTURE2019_WINDOW_H

#include <string>
#include <sstream>

namespace adventure::view {
    class Window {
    public:
        virtual ~Window() = default;

        virtual void update() = 0;

        virtual void redraw() = 0;

        virtual void displayText(const std::string &text) = 0;

        inline void displayText(const std::ostringstream &os) {displayText(os.str());};
    };
}

#endif //ADVENTURE2019_WINDOW_H
