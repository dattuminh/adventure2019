#ifndef CMPT373_PROJECT_RESET_H
#define CMPT373_PROJECT_RESET_H

#include "Door.h"
#include "ID.h"

#include <optional>
#include <string>

namespace adventure::model {

    BETTER_ENUM(ResetAction, int, NPC, Give, Equip, Object, Door, Put, Nil)

/*
 * Reset is responsible for handling information regarding the actions
 * taken by the users
 */
    class Reset {
    public:
        typedef ResetAction Action;

        Reset() = default;

        void setAction(Action a) { this->action = a; };

        Action getAction() const { return this->action; };

        void setId(Id id) { this->id = id; };

        Id getId() const { return this->id; };

        void setLimit(int limit) { this->limit = limit; };

        std::optional<int> getLimit() const { return this->limit; };

        void setRoom(RoomId room) { this->room = room; };

        std::optional<RoomId> getRoom() const { return this->room; };

        void setSlot(int slot) { this->slot = slot; };

        std::optional<int> getSlot() const { return this->slot; };

        void setState(const Door::State &state) { this->state = state; };

        std::optional<Door::State> getState() const { return this->state; };

        void setContainer(Id c){ this->container = c; }

        std::optional<Id> getContainer() const { return this->container; }

        friend std::ostream &operator<<(std::ostream &os, const Reset &r);

    private:
        Action action{Action::Nil};
        Id id;
        std::optional<int> limit;
        std::optional<RoomId> room;
        std::optional<int> slot;
        std::optional<Door::State> state;
        std::optional<Id> container;
    };
}

#endif // CMPT373_PROJECT_RESET_H
