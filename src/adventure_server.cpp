#include "GameServer.h"

// Main method for invoking server
int
main(int argc, char *argv[]) {
    FLAGS_logtostderr = true; // Set glog to log std::cerr and not to a file
    google::InitGoogleLogging(argv[0]);
    google::InstallFailureSignalHandler();

    if (argc < 3) {
        LOG(FATAL) << "Usage: " << argv[0] << " <port> <data dir>\n"
                  << "  e.g. " << argv[0] << " 4002 ../data";
        return 1;
    }

    // TODO: Code for basic sanity check of passed in args
    // ex: port range, port availability, existence of data file

    const unsigned int DEFAULT_CONNECTIONS_MAX = 10;
    unsigned short port = std::stoi(argv[1]);

    adventure::server::GameServer server {port, argv[2], DEFAULT_CONNECTIONS_MAX};

    return server.runServer();
}