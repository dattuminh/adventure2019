#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Item.h"
#include "ID.h"
#include <string>

using namespace adventure::model;

TEST(ItemTest, testId){
    Item item1;

    Id id1 = 325;

    adventure::ItemId itemId1{id1};

    item1.setId(itemId1);
    
    item1.setContents({itemId1});

    EXPECT_EQ(itemId1, item1.getId());
}

TEST(ItemTest, TestContainer){
    Item item1;

    Id id1 = 325;

    adventure::ItemId itemId1{id1};

    item1.setId(itemId1);

    item1.setContents({itemId1});

    EXPECT_EQ(true, item1.isContainer());
    EXPECT_EQ(1, item1.getContentSize());
    EXPECT_EQ(true, item1.itemExistInContainer(itemId1));
}
