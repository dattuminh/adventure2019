#include "GameClient.h"

namespace adventure::client {
    void GameClient::runGame() {
        while (connected()) {
            try {
                client.update();
            } catch (std::exception &e) {
                currentWindow->displayText("Exception from Client update:");
                currentWindow->displayText(e.what());
            }

            auto response = this->client.receive();
            if (!response.empty()) {
                processResponse(response);
            }
            currentWindow->update();
        }
    }

    GameClient::GameClient(const char *address, const char *port) :
    client{address, port} {
        windows.emplace(CHAT, std::make_unique<ChatWindow>([this](auto c) { this->onChatEntry(c); }));
        windows.emplace(LOGIN, std::make_unique<AuthenticationWindow>([this](auto u, auto p) { this->onAuthEntry(u, p); }));
    }

    void GameClient::clientReceiveCommand(std::string &commandStr) {
        if(!inputChecker.isValidCommandType(commandStr)) {
            currentWindow->displayText("Invalid command: " + commandStr + "\n");
            return;
        }
        try{
            auto jString = inputChecker.chatInputToJsonString(commandStr);
            client.send(jString);
        }catch (const std::runtime_error &e){
            std::ostringstream os;
            os << "argument exception: " << e.what() << std::endl
               << "invalid argument in command: " << commandStr << std::endl;
            currentWindow->displayText(os);
        }
    }

    void GameClient::processResponse(std::string &message) {

        try {
            json j = json::parse(message);
            Action action = getAction(j);

            switch (action) {
                case Action::Authenticate: {
                    auto a = j.get<AuthResponse>();
                    processAuthMessage(a);
                    break;
                }
                case Action::ChatMessage: {
                    ChatMessage msg;
                    from_json(j, msg);
                    currentWindow->displayText(msg.from + ": " + msg.text + "\n");
                    break;
                }
                case Action::Model:{
                    auto m = j.get<ModelData>();
                    processModelData(m);
                    break;
                }
                case Action::Status:{
                    auto stat = j.get<ServerStatus>();
                    processServerStat(stat);
                    break;
                }
                case Action::Error:{
                    auto error = j.get<ErrorMessage>();
                    std::ostringstream os;
                    os << "Error on " << error.type._to_string() << std::endl
                       << error.message << std::endl;
                    currentWindow->displayText(os);
                }
                case Action::Confirm: {
                    processConfirm(model::get_enum<ModelType>(j, "type"), j);
                    break;
                }
                default:
                    break;
            }

        } catch (const json::exception &e) {
            //debugging
            //currentWindow->displayText("Json exception: " + message + "\n");
            currentWindow->displayText(message); // TODO: Temporary - for responses of pure string (ie heartbeat)
        }
    }

    void GameClient::processConfirm(ModelType type, json j){
        switch (type){
            case ModelType::Object:{
                auto confirm = j.get<model::Confirmation<Object>>();
                auto models = confirm.options;
                // process or display objects
            }
            case ModelType::Character:{
                auto confirm = j.get<model::Confirmation<std::string>>();
                auto models = confirm.options;
            }
        }
    }

    void GameClient::processModelData(const ModelData &model){
        switch (model.type) {
            case ModelType::Area: {
                auto area = model.data.get<Area>();
                processAreaModel(area);
                break;
            }
            case ModelType::Room: {
                auto room = model.data.get<Room>();
                processRoomModel(room);
                break;
            }
            case ModelType::Door: {
                auto door = model.data.get<Door>();
                processDoorModel(door);
                break;
            }
            case ModelType::NPC: {
                auto npc = model.data.get<NPC>();
                processNpcModel(npc);
                break;
            }
            case ModelType::Object: {
                auto object = model.data.get<Object>();
                processObjectModel(object);
                break;
            }
            case ModelType::Extra: {
                auto extra = model.data.get<Extra>();
                processExtraModel(extra);
                break;
            }
            default:
            case ModelType::Character:
            case ModelType::Inventory:
            case ModelType::Item:
            case ModelType::Reset:
            case ModelType::Shop:
            case ModelType::User:
            case ModelType::Nil:{
                std::cerr << "Parsing this model is not implemented yet\n";
            }
        }
    }

    void GameClient::processRoomModel(const Room &room) {
        std::ostringstream os;
        os << room.getName() << std::endl
           << room.getDesc() << std::endl;

        os << std::endl << "Doors: " << std::endl;
        for(const auto &door : room.getDoors()) {
            processDoorModel(door);
        }
        os << std::endl;

        currentWindow->displayText(os);
    }

    void GameClient::processDoorModel(const Door &door) {
        std::ostringstream os;
        os << "Direction: " << door.getDirection()._to_string() << std::endl
           << "State: " << door.getState()->_to_string() << std::endl
           << "To: " << door.getTo() << std::endl
           << std::endl;

        currentWindow->displayText(os);
    }

    void GameClient::processAreaModel(const Area &area) {
        std::ostringstream os;
        os << area.getName() << std::endl;

        os << std::endl << "NPCs: " << std::endl;
        for(const auto &npc : area.getNpcs()) {
            processNpcModel(npc);
        }

        os << std::endl << "Rooms: " << std::endl;
        for(const auto &room : area.getRooms()) {
            processRoomModel(room);
        }

        os << std::endl << "Objects: " << std::endl;
        for(const auto &object : area.getObjects()) {
            processObjectModel(object);
        }

        currentWindow->displayText(os);
    }

    void GameClient::processObjectModel(const Object &object) {
        std::ostringstream os;
        os << "Keywords: " << std::endl;
        for(const auto &keyword : object.getKeywords()) {
            os << keyword << std::endl;
        }

        for(const auto &longdesc : object.getLongDesc()) {
            os << longdesc << std::endl;
        }
        os << std::endl;

        currentWindow->displayText(os);
    }

    void GameClient::processNpcModel(const NPC &npc) {
        std::ostringstream os;
        os << "Keywords: " << std::endl;
        for(const auto &keyword : npc.getKeywords()) {
            os << keyword << std::endl;
        }
        os << std::endl << npc.getDescription() << std::endl
           << std::endl;

        currentWindow->displayText(os);
    }

    void GameClient::processExtraModel(const Extra &extra) {
        std::ostringstream os;
        os << "Keywords: " << std::endl;
        for(const auto &keyword : extra.getKeywords()) {
            os << keyword << std::endl;
        }
        os << std::endl << extra.getDescription() << std::endl
           << std::endl;

        currentWindow->displayText(os);
    }


    void GameClient::processAuthMessage(const AuthResponse &response) {
        if(response.authenticated) {
            callWindow(CHAT);
            if(response.action == +Action::Authenticate) {
                currentWindow->displayText("Logged in as: " + response.text + "\n");
            } else {
                currentWindow->displayText("Registered as: " + response.text + "\n");
            }
        } else {
            currentWindow->displayText(response.text + "\n");
        }
    }

    void GameClient::processServerStat(const ServerStatus &stat){
        std::ostringstream os;
        os << "Areas: " << std::endl;
        for(auto area : stat.areas){
            os << area << std::endl;
        }
        os << std::endl << "Active Players: " << std::endl;
        for(auto player : stat.connected_players){
            os << player << " ";
        }
        os << std::endl
           << "Max Players: " << stat.max_players << std::endl
           << "Player Count: " << stat.player_count << std::endl;
        currentWindow->displayText(os);
    }

    void GameClient::callWindow(WindowType type) {
        auto found = windows.find(type);
        if(windows.end() != found) {
            currentWindow = (found->second).get();
            currentWindow->redraw();
        }
    }

    void GameClient::onChatEntry(std::string text){
        if ("exit" == text || "quit" == text) {
            exitClient();
        } else {
            clientReceiveCommand(text);
        }
    }

    void GameClient::onAuthEntry(std::string username, std::string password) {
        if (username.empty()) {
            currentWindow->displayText("Enter a valid username");
        } else if (password.empty()) {
            currentWindow->displayText("Enter a valid password");
        } else {
            auto jString = inputChecker.authInputToJsonString(username, password);
            client.send(jString);
        }
    }

    void GameClient::exitClient() {
        exit(-1);
    }
}
