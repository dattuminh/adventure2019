#ifndef ADVENTURE2019_CONTROLLER_H
#define ADVENTURE2019_CONTROLLER_H

#include "Parser.h"
#include "StructModels.h"
#include "ID.h"

#include <memory>
#include <list>


namespace adventure::controller {
    using json = nlohmann::json;

    using adventure::CharId;

    using adventure::model::Id;
    using adventure::model::GameDataWrapper;
    using adventure::model::Command;
    using adventure::model::ChatMessage;
    using adventure::model::ChatCommand;
    using adventure::model::Object;
    using adventure::model::Item;
    using adventure::model::Inventory;
    using adventure::model::IdHasher;
    /**
     * Controllers that deal with Users directly should use this. For example 
     * when the User joins the game, they have not yet been assigned a 
     * Character, and therefore cannot be sent data through the ResponseMap.
     */
    using ResponseDataList = std::weak_ptr<std::list<model::GameDataWrapper>>;

    /**
     * Controllers that interact with Characters should use this to send data to
     * the Character, which in turn is sent to the User.
     */ 
    using ResponseMap = std::shared_ptr<std::unordered_map<CharId, std::list<json>, StrongAliasIdHasher>>;

    template <typename T>
    struct CCommand{
        CharId id{};
        T data;
    };
}

#endif //ADVENTURE2019_CONTROLLER_H
