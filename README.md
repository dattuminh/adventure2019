# adventure2019

## Cloning for the first time
`git clone --recursive git@csil-git1.cs.surrey.sfu.ca:373-19-1-dacquoise/adventure2019.git`
CMake will automatically update the submodules for you.

## Building
`cmake ../path/to/project/`
`make`

### Optional CMake flags
`cmake -DCMAKE_CXX_COMPILER=clang++`
CMAKE_EXPORT_COMPILE_COMMANDS is enabled by default.
`cmake -G Ninja`
This enables ninja-build. You can then build with `ninja`.

### clang-tidy integration
The CMake scripts run clang-tidy if you have it installed.
`sudo apt install clang-tidy clang`

### Useful build commands
`make clean`
