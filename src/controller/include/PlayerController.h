#ifndef ADVENTURE2019_PLAYERCONTROLLER_H
#define ADVENTURE2019_PLAYERCONTROLLER_H

#include "Player.h"
#include "Controller.h"
#include "ID.h"
#include "CharacterController.h"
#include "Parser.h"
#include "Character.h"
#include "StructModels.h"

#include <unordered_map>
#include <string>
#include <stdexcept>
#include <algorithm>
#include <utility>
#include <memory>

namespace adventure::controller {
    using adventure::model::Player;

    class PlayerController {

    public:
        PlayerController(std::weak_ptr<CharacterController> c, ResponseDataList out) :
                charCtrl{std::move(c)}, outgoing{std::move(out)} {};

        ~PlayerController() = default;

        // Mutators

        CharId addPlayer(const std::string &username);

        // Accessors

        const Player &getPlayer(const std::string &name) const;

        bool characterIsAvatar(CharId id) const;

        std::string avatarToPlayerName(CharId id) const;

        CharId getActiveAvatar(const std::string &name) const;

    private:
        std::weak_ptr<CharacterController> charCtrl;

        ResponseDataList outgoing;

        Player &getMutablePlayer(const std::string &);

        std::unordered_map<std::string, Player> players{};

        std::unordered_map<CharId, std::string, StrongAliasIdHasher> avatars{};
    };
}

#endif //ADVENTURE2019_PLAYERCONTROLLER_H
