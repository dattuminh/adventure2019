#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "NPC.h"
#include "ID.h"
#include <string>

using namespace adventure::model;

// Tests NPC ID
TEST (NPCTest, testingNpcId){
    Id id = 4000;

    adventure::NPCId npcId{id};
    NPC Npc;

    Npc.setId(npcId);
    EXPECT_EQ(npcId, Npc.getId());
}

// Tests NPC Descriptions
TEST (NPCTest, testingDescription){
    std::string shortdesc = "testing";
    std::vector<std::string> keywords = {"test1", "test2"};
    std::vector<std::string> longdesc = {"long testing", "long testing2"};
    std::string description = "testing description";
    NPC Npc;

    Npc.setShortDesc(shortdesc);
    Npc.setKeywords(keywords);
    Npc.setDescription(description);
    Npc.setLongDesc(longdesc);

    EXPECT_EQ(shortdesc, Npc.getShortDesc());
    EXPECT_EQ(keywords, Npc.getKeywords());
    EXPECT_EQ(description, Npc.getDescription());
    EXPECT_EQ(longdesc, Npc.getLongDesc());
}