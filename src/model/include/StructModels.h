#ifndef ADVENTURE2019_STRUCTMODELS_H
#define ADVENTURE2019_STRUCTMODELS_H

#include "Adventure.h"

#include <optional>
#include <string>
#include <vector>
#include "ID.h"

namespace adventure::model {
    struct ServerStatus {
        unsigned long player_count{};
        long max_players{};
        std::vector<std::string> connected_players{};
        std::vector<std::string> areas{};
        constexpr static Action action{Action::Status};
    };

    struct ChatMessage {
        std::string from;
        std::string text;
        constexpr static Action action{Action::ChatMessage};
    };

    // Use for commands without specialized type.
    struct GenericCommand {
        std::string command;
        std::vector<std::string> args;
    };

    struct Move {
        Direction direction{Direction::Nil};
        constexpr static Action action{Action::Move};
    };

    struct InventoryCommand {
        InventoryAction type{InventoryAction::put};
        std::string item;
        std::string container;
        std::string giveTo;
        SlotLocation slot{SlotLocation::about};
        constexpr static Action action{Action::Inventory};
    };

    /*
     * Wrapper class to pass request/response data. This is to abstract the concept of "connections" away from the core
     * game logic. On the server side there is a facility to map a 'connection identifier' to an actual Connection.
     * On the game side this identifier will represent a player.
     */
    struct GameDataWrapper {
        std::string connection_identifier; // For now this is the username of the player.
        json data;
    };

    std::ostream &operator<<(std::ostream &os, const GameDataWrapper &g);

    struct Command {
        std::string sender;
        std::string command;
        std::string input;
        Action action{Action::Nil};
    };

    struct Debug {
        DebugCmd debugCmd{DebugCmd::dump};
        constexpr static Action action = Action::Debug;
    };

    struct JoinArea {
        std::string area;
        constexpr static Action action{Action::Join};
    };

    struct ModelData {
        ModelType type{ModelType::Nil};
        json data;
        constexpr static Action action{Action::Model};
    };

    struct Authenticate {
        std::string username;
        constexpr static Action action{Action::Authenticate};
    };

    struct ChatCommand {
        std::string text{};
        CommCommands type{CommCommands::says};
        std::optional<std::string> to;
        constexpr static Action action{Action::ChatCommand};
    };

    struct LookCommand {
        std::string arg;
        LookAction type{LookAction::look};
        constexpr static Action action{Action::Look};
    };

    struct ErrorMessage {
        std::string message;
        ModelType type{ModelType::Nil};
        constexpr static Action action{Action::Error};

    };

    template<class Model>
    struct Confirmation{
        std::vector<Model> options;
        ModelType type{ModelType::Nil};
        constexpr static Action action{Action::Confirm};
    };

    struct AuthResponse {
        bool authenticated;
        std::string text;
        Action action{Action::Nil};
    };
}

#endif //ADVENTURE2019_STRUCTMODELS_H
