#ifndef ADVENTURE2019_INVCOMMANDCONTROLLER_H
#define ADVENTURE2019_INVCOMMANDCONTROLLER_H

#include "RoomController.h"
#include "CharacterController.h"
#include "Controller.h"
#include "InventoryController.h"

namespace adventure::controller{
class InvCommandController {
public:
    InvCommandController(std::weak_ptr<RoomController> room,
                         std::weak_ptr<CharacterController> character,
                         std::weak_ptr<InventoryController> inventory,
                         ResponseMap out);
    void handleInventoryCommand(const CCommand<InventoryCommand> &command);
private:
    std::weak_ptr<RoomController> roomCtrl;
    std::weak_ptr<CharacterController> charCtrl;
    std::weak_ptr<InventoryController> invCtrl;
    ResponseMap outgoing;

    void putCommand(const CCommand<InventoryCommand> &command);

    void takeCommand(const CCommand <InventoryCommand> &command);

    void dropCommand(const CCommand <InventoryCommand> &command);

    void sendData(CharId alias, const json &message);

    model::ErrorMessage generateNotOwnedError(std::string_view item);
    model::ErrorMessage generateNotInRoomError(std::string_view item);

    void wearCommand(const CCommand <InventoryCommand> &command);

    void removeCommand(const CCommand <InventoryCommand> &command);

    void giveCommand(const CCommand <InventoryCommand> &command);

    model::ErrorMessage generateCannotFindChar(std::string_view charName);

    model::Confirmation<Object> generateConfirmationResponse(const std::vector<ObjectType> &types);
};
}


#endif //ADVENTURE2019_INVCOMMANDCONTROLLER_H
