#ifndef ADVENTURE2019_AREACONTROLLER_H
#define ADVENTURE2019_AREACONTROLLER_H

#include "Controller.h"
#include "Area.h"
#include "CommandController.h"
#include "RoomController.h"
#include "ResetController.h"
#include "LookController.h"
#include "CommandController.h"
#include "StructModels.h"
#include "Room.h"
#include "MovementController.h"
#include "PlayerController.h"
#include "InventoryController.h"
#include "InvCommandController.h"

#include <string>
#include <vector>
#include <chrono>
#include <memory>

namespace adventure::controller {
    using adventure::model::GameDataWrapper;

    class AreaController {
    public:
        AreaController(model::Area a);
        ~AreaController() = default;

        void receiveIncoming(GameDataWrapper data);

        std::list<GameDataWrapper> &getOutgoing();

        void performAreaTick(std::chrono::seconds time_delta);

    private:
        std::shared_ptr<std::list<GameDataWrapper>> outgoing;
        ResponseMap responseMap;
        std::shared_ptr<CommandController> comHandler;
        std::shared_ptr<RoomController> roomController;
        std::shared_ptr<CharacterController> characterController;
        std::shared_ptr<MovementController> moveController;
        std::shared_ptr<LookController> lookController;
        std::shared_ptr<CommunicationController> commController;
        std::shared_ptr<PlayerController> playerController;
        std::shared_ptr<InventoryController> inventoryController;
        std::shared_ptr<InvCommandController> invCommandController;

        model::Area model;

        std::unique_ptr<ResetController> resetController;

        const RoomId default_room;
    };
}

#endif //ADVENTURE2019_AREACONTROLLER_H
