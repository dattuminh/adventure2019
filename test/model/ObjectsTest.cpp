#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Object.h"
#include "ID.h"
#include "Extra.h"
#include <string>
#include <vector>

using namespace adventure::model;
using adventure::ObjectType;

// Tests id, shortDesc, keywords, longDesc
TEST(ObjectTest, testingObject) {
    ObjectType id{20};
    std::string shortdesc = "cheers";
    std::vector<std::string> keywords = {"box", "jacket"};
    std::vector<std::string> longdesc = {"google", "testing"};

    Object object;  
    object.setId(id);
    object.setShortDesc(shortdesc);
    object.setKeywords(keywords);
    object.setLongDesc(longdesc);

    EXPECT_EQ(id, object.getId());
    EXPECT_EQ(shortdesc, object.getShortDesc());
    EXPECT_EQ(keywords, object.getKeywords());
    EXPECT_EQ(longdesc, object.getLongDesc());
}       

// Tests Extra for keywords and description
TEST(ObjectTest, testingExtra) {
    std::vector<std::string> keywords = {"box", "jacket"};

    Object object; 
    Extra extra;
    extra.setKeywords(keywords);
    extra.setDescription("string");
    std::vector<Extra> extras = {extra};

    object.setExtra(extras);
    EXPECT_TRUE(object.getExtra().size() == 1);
    EXPECT_EQ(extras[0].getKeywords(), object.getExtra()[0].getKeywords());
    EXPECT_EQ(extras[0].getDescription(), object.getExtra()[0].getDescription());
}