#include "InventoryController.h"

namespace adventure::controller{

    InventoryController::InventoryController(const std::vector<Object> &objects) {
        for (auto &o : objects) {
            objectMap.emplace(o.getId(), std::move(o));
        }
    }

    InventoryId InventoryController::createInventory(){
        InventoryId id{inv_id++};
        Inventory inventory;
        inventoryMap.emplace(id, inventory);
        return id;
    }

    void InventoryController::moveItem(ItemId itemId, InventoryId oldInventoryId, InventoryId newInventoryId){
        auto item = removeItem(itemId, oldInventoryId);
        if(item.isContainer()){
            auto container = item.getMutableContainer();
            auto containerIds = container.getAllItemIds();
            std::for_each(containerIds.begin(), containerIds.end(), [&](ItemId id){
                auto containerItem = removeItem(id, oldInventoryId);
                addItem(item, newInventoryId);
            });
        }

        addItem(item, newInventoryId);

    }
    void InventoryController::addItem(const Item &item, InventoryId inventoryId){
        auto inventory = inventoryMap.find(inventoryId)->second;
        inventory.addItem(item);
    }

    const Item &InventoryController::getItem(ItemId itemId, InventoryId inventoryId) const{
        auto inventory = inventoryMap.at(inventoryId);
        return inventory.getItem(itemId);
    }

    Item InventoryController::removeItem(ItemId itemId, InventoryId inventoryId){
        auto inventory = inventoryMap.find(inventoryId)->second;
        return inventory.removeItem(itemId);
    }

    const Object &InventoryController::getObjectByItemId(ItemId itemId, InventoryId inventoryId) const{
        if(!isItemInInventory(itemId, inventoryId)){
            throw std::runtime_error("Item is not in inventory");
        }
        auto inventory = inventoryMap.find(inventoryId)->second;
        auto objType = inventory.getObjectType(itemId);
        return objectMap.at(objType);
    }
    bool InventoryController::isItemInInventory(ItemId itemId, InventoryId inventoryId) const{
        auto inventory = inventoryMap.at(inventoryId);
        return inventory.itemInInventory(itemId);
    }

    bool InventoryController::isObjectInInventory(ObjectType objectType, InventoryId inventoryId) const{
        auto inventory = inventoryMap.at(inventoryId);
        return inventory.objectInInventory(objectType);
    }

    bool InventoryController::isObjectKeywordInInventory(std::string_view keyword, InventoryId inventoryId) const{
        if(!isObjectKeywordExist(keyword)){
            return false;
        }
        auto inventory = inventoryMap.at(inventoryId);
        auto objType = getObjectTypeByKeyword(keyword);
        return inventory.objectInInventory(objType);
    }

    bool InventoryController::isObjectKeywordExist(std::string_view keyword) const{
        auto objItr = std::find_if(objectMap.begin(), objectMap.end(), [&](const auto &objPair){
            auto keywords = objPair.second.getKeywords();
            auto combinedKeyword = boost::join(keywords, " ");
            return combinedKeyword.compare(keyword) == 0;
        });
        return objItr != objectMap.end();
    }

    ObjectType InventoryController::getObjectTypeByKeyword(std::string_view keyword) const{
        auto objItr = std::find_if(objectMap.begin(), objectMap.end(), [&](const auto &objPair){
            auto keywords = objPair.second.getKeywords();
            auto combinedKeyword = boost::join(keywords, " ");
            return combinedKeyword.compare(keyword) == 0;
        });
        return objItr->first;
    }

    const Object &InventoryController::getFirstObjectByKeyword(std::string_view keyword) const{
        auto objType = getObjectTypeByKeyword(keyword);
        return objectMap.at(objType);
    }

    ItemId InventoryController::getItemIdByKeyword(std::string_view keyword, InventoryId inventoryId) const {
        auto inventory = inventoryMap.at(inventoryId);
        auto objType = getObjectTypeByKeyword(keyword);
        auto item = inventory.getItem(objType);
        return item.getId();
    }

    const Object &InventoryController::getObject(ObjectType type) const{
        return objectMap.at(type);
    }
    std::vector<ObjectType> InventoryController::getAllObjectTypesByKeyword(std::string_view keyword,
                                                                            InventoryId inventoryId){
        std::vector<ObjectType> types;
        auto inventory = inventoryMap.at(inventoryId);
        for(const auto &[type, object] : objectMap){
            auto keywords = object.getKeywords();
            if(std::find(keywords.begin(), keywords.end(), keyword) != keywords.end()){
                types.emplace_back(type);
            }
        }

        return types;
    }

    ItemId InventoryController::addNewItem(ObjectType type, InventoryId inventoryId) {
        /**
         * TODO: Implement
         * Create a new instance of an Item with ObjectType then add to the inventory Return the ItemID for the new item
         */

        LOG(ERROR) << "Not implemented";
        return ItemId{0};
    }



    const std::vector<Item> &InventoryController::getItemsInInventory(InventoryId inventoryId) const {
        LOG(ERROR) << "Not implemented getITemsInINventory()";
        static std::vector<Item> temp(1); // TODO: remove me
        return temp;
    }

    const Item &InventoryController::getItem(ItemId itemId) const {
        for(const auto &[_, inv] : inventoryMap){
            if(inv.itemInInventory(itemId)){
                return inv.getItem(itemId);
            }
        }
        throw std::runtime_error("Trying to look for an Item that does not exist");
    }

    void InventoryController::addNewItemToContainer(ObjectType type, ItemId container) {
        LOG(ERROR) << "Not implemented addNewItemToContainer()";
    }

    void InventoryController::putItemInContainer(ItemId container, ItemId item, InventoryId inventoryId){
        auto contents = getMutableItem(container, inventoryId).getMutableContainer();
        contents.addObject(item);
    }

    Item &InventoryController::getMutableItem(ItemId itemId, InventoryId inventoryId) {
        auto inventory = inventoryMap.at(inventoryId);
        return inventory.getMutableItem(itemId);
    }


    void InventoryController::equipItem(const Item &item, SlotLocation slot, CharId charId){
        auto equipments = equipMap.at(charId);
        equipments[slot._to_integral()] = item;
    }

    Item InventoryController::unequipItem(SlotLocation slot, CharId charId){
        auto equipments = equipMap.at(charId);

        auto copy = equipments[slot._to_integral()];
        equipments[slot._to_integral()] = std::nullopt;
        return copy.value();
    }

    bool InventoryController::isSlotEmpty(SlotLocation slot, CharId charId){
        auto equipments = equipMap.at(charId);
        return equipments[slot._to_integral()] == std::nullopt;
    }

}