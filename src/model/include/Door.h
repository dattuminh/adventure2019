#ifndef ADVENTURE2019_DOOR_H
#define ADVENTURE2019_DOOR_H

#include "Adventure.h"
#include "ID.h"

#include <iostream>
#include <string>
#include <vector>

namespace adventure::model {

    BETTER_ENUM(DoorState, int, Open, Closed, Locked)

    class Door {
    public:
        typedef ::better_enums::optional<DoorState> State;

        Door() = default;

        virtual ~Door() = default;

    private:
        Direction direction{Direction::Nil};
        State state{DoorState::Open};
        std::string description;
        std::vector<std::string> keywords;
        RoomId to;

    public:
        const Direction &getDirection() const { return direction; }

        void setDirection(const Direction &value) { this->direction = value; }

        const State &getState() const { return state; }

        void setState(const State &value) { this->state = value; }

        const std::string &getDescription() const { return description; }

        void setDescription(const std::string &description) { this->description = description; }

        const std::vector<std::string> &getKeywords() const { return keywords; }

        void setKeywords(const std::vector<std::string> &value) { this->keywords = value; }

        RoomId getTo() const { return to; }

        void setTo(RoomId value) { this->to = value; }

    };

    inline bool isEqual(std::vector<std::string> v1, std::vector<std::string> v2){
        return (v1.size() == v2.size() && std::equal(v1.begin(), v1.end(), v2.begin()));
    }

    inline bool operator==(const Door& test, const Door& other){
        return test.getDirection()._to_string() == other.getDirection()._to_string()
            && test.getState()->_to_string() == other.getState()->_to_string()
            && test.getDescription() == other.getDescription()
            && isEqual(test.getKeywords(), other.getKeywords())
            && test.getTo() == other.getTo();
    }

    inline std::ostream &operator<<(std::ostream &os, const Door &g) {
        return os << "Door {.direction: " << g.getDescription() << " .state: "
                  << (g.getState() ? g.getState()->_to_string() : "Not Initialized") << " .description: "
                  << g.getDescription() << " .to: " << g.getTo() << " }";
    }
}

#endif //ADVENTURE2019_DOOR_H
