#ifndef ADVENTURE2019_EXT_H
#define ADVENTURE2019_EXT_H

#include <vector>
#include <string>

namespace adventure::model {

    class Extra {
    public:
        Extra() = default;

        ~Extra() = default;

    private:
        std::vector<std::string> keywords;
        std::string desc;

    public:
        const std::vector<std::string> &getKeywords() const { return keywords; }

        void setKeywords(const std::vector<std::string> &value) { this->keywords = value; }

        const std::string &getDescription() const { return desc; }

        void setDescription(const std::string &value) { this->desc = value; }
    };
}

#endif //ADVENTURE2019_EXT_H
