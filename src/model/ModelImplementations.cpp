#include "Area.h"
#include "Parser.h"

using namespace adventure::model;
using adventure::CharId;

std::ostream &adventure::model::operator<<(std::ostream &os, const adventure::model::Reset &r) {
    os << "Reset ID: " << r.id << ", Action: " << r.action._to_string() << ", Room ID: ";
    if (r.room) {
        os << *r.room;
    } else {
        os << "none";
    }
    os << ", Slot: ";
    if (r.slot) {
        os << *r.slot;
    } else {
        os << "none";
    }
    os << ", Door State: " << (r.state ? r.state.value()->_to_string() : "No state") << ", Container ID: ";
    if (r.container) {
        os << *r.container;
    } else {
        os << "none";
    }
    return os;
}

std::ostream &adventure::model::operator<<(std::ostream &os, const adventure::model::GameDataWrapper &g) {
    return os << "GameDataModel { .connection_identifier: " << g.connection_identifier << " .data: "
              << g.data.dump(DEBUG_JSON_INDENT) << '}';
}

/**
 * Resets like Give, Put, and Equip refer to the last NPC or Object. We need to group them together so we can reset them
 * as one unit.
 * @param resets
 */
void Area::setResets(const std::vector<Reset> &resets) {

    static constexpr std::array<Reset::Action, 3> const special_resets{Reset::Action::NPC,
                                                                       Reset::Action::Object,
                                                                       Reset::Action::Door
    };

    this->reset_groups.reserve(resets.size()); // Number of groups cannot exceed number of individual Resets

    auto itr = resets.begin();

    while (itr != resets.end()) {
        std::vector<Reset> current_set{};
        auto inserter = std::back_inserter(current_set);

        inserter = *itr++;

        auto next_special = std::find_first_of(
                itr, resets.end(), special_resets.begin(), special_resets.end(),
                [](const Reset &next, const Reset::Action &special) { return next.getAction() == special; });

        current_set.reserve(std::distance(itr, next_special));

        while (itr != next_special && itr != resets.end()) {
            inserter = *itr++;
        }

        this->reset_groups.emplace_back(std::move(current_set));
    }

    this->reset_groups.shrink_to_fit();
}

std::vector<CharId> NPC::getCharacters(RoomId room_id) const {
    std::vector<CharId> characters_in_room;
    characters_in_room.reserve(characters.size());

    for (const auto &pair : characters) {
        if (pair.second == room_id) {
            characters_in_room.emplace_back(pair.first);
        }
    }
    characters_in_room.shrink_to_fit();
    return characters_in_room;
}
