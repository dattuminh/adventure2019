#ifndef ADVENTURE2019_GAMESERVER_H
#define ADVENTURE2019_GAMESERVER_H

#include "GameController.h"
#include "Authentication.h"

#include <Server.h>
#include <utility>
#include <vector>
#include <string>
#include <memory>
#include <map>

namespace adventure::server {

    using adventure::model::getAction;
    using adventure::model::AuthResponse;
    using adventure::model::ServerStatus;
    using json = nlohmann::json;
    using networking::Server;
    using networking::Connection;
    using networking::Message;

    class GameServer {
    public:
        GameServer(unsigned short port, const char *data_dir, long max_connections);

        int runServer();

    private:
        std::unique_ptr<Server> server;
        std::vector<Connection> clients;
        std::unique_ptr<controller::GameController> game;
        std::unique_ptr<AuthenticationHandler> authHandler;
        std::deque<Message> outgoing;
        std::deque<Message> incoming;

        long max_connections;

        void onConnect(Connection);

        void onDisconnect(Connection);

        void processMessages(std::deque<Message> &incoming_messages);

        void disconnectClients();

        void debugAction(const json &, Connection);

        void unauthenticatedMessageHandler(const Message &, const json &);

        json parseMessageToJson(const Message &);

        void processAction(const Message &msg, const json &j);

        bool syntaxCheckJsonAction(const json &json);

        void prepareOutgoingFromGame();

        void processHeartbeat(Connection);

        void statusRequest(Connection);

        void tryServerUpdate();

        void sendChatMessage(Connection c, const std::string &msg);
    };
}

#endif //ADVENTURE2019_GAMESERVER_H
