#include "GameClient.h"

using adventure::client::WindowType;

int main(int argc, char *argv[]) {
    std::string address, port;
    if (argc == 3) {
        address = argv[1];
        port = argv[2];
    } else {
        std::cout << "Please enter your IP address (e.g. 'localhost'): ";
        std::cin >> address;

        std::cout << "Please enter a port number (e.g. '8080'): ";
        std::cin >> port;
    }

    bool valid = false;
    while (!valid) {
        try {
            adventure::client::GameClient client{address.c_str(), port.c_str()};

            client.callWindow(WindowType::LOGIN);
            client.runGame();

            return 0;
        } catch (boost::system::system_error &e) {
            std::cout << "\nInvalid IP address or port.\n";
            std::cout << "Please enter a valid IP address (e.g. 'localhost'): ";
            std::cin >> address;

            std::cout << "Please enter a valid port number (e.g. '8080'): ";
            std::cin >> port;
        }
    }
}