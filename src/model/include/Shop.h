#ifndef ADVENTURE2019_SHOP_H
#define ADVENTURE2019_SHOP_H

#include "Item.h"
#include "StructModels.h"

#include <string>
#include <vector>

namespace adventure::model {
    class Shop {
    public:
        void addItem(std::string itemName, double itemPrice, std::string itemDescription);

        Item getItem(std::string name);

    private:
        std::vector<Item> store;
        Id id;
    };
}

#endif //ADVENTURE2019_SHOP_H