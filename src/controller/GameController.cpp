#include "GameController.h"

namespace adventure::controller {

    ServerStatus GameController::serverStatus() const {
        ServerStatus s{};

        s.connected_players.reserve(s.player_count);
        for (const auto &[player, _] : playerToArea) {
            s.connected_players.push_back(player);
        }

        s.areas.reserve(areas.size());
        for (const auto &[area_name, _] : areas) {
            s.areas.push_back(area_name);
        }

        return s;
    }

    void GameController::receiveGameData(GameDataWrapper message) {
        // TODO: Implement this
        if (playerToArea.find(message.connection_identifier) == playerToArea.end()) {
            // TODO: THIS IS AN INCORRECT ASSUMPTION OF DATA ALWAYS BEING JOINAREA
            playerJoinArea(message);
        } else {
            auto keyToAreaForPlayer = playerToArea.at(message.connection_identifier); // TODO: need better name for this
            areas.at(keyToAreaForPlayer).receiveIncoming(message);
        }
    }

    std::list<GameDataWrapper> GameController::getOutgoing() {
        std::list<GameDataWrapper> out{};

        out.splice(out.end(), this->outgoing);

        for (auto &[_, area] : areas) {
            out.splice(out.end(), area.getOutgoing());
        }

        return out;
    }

    /*
     * Each game loop the Server will call this method. This method calculates the time elapsed since the last tick
     * and will pass the delta time to each area for further processing. Each receiver of the time delta can then decide
     * on their own if enough time has passed for their events (Resets) to trigger.
     */
    void GameController::performGameTick() {
        std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();

        auto time_delta = std::chrono::duration_cast<std::chrono::seconds>(now - last_tick);

        VLOG(1) << "Time since last tick: " << time_delta.count() << "seconds.\n";

        for (auto &[_, area] : areas) {
            area.performAreaTick(time_delta);
        }

        last_tick = std::chrono::steady_clock::now();


        VLOG(1) << "Time to process tick: "
                << std::chrono::duration_cast<std::chrono::seconds>(last_tick - now).count() << " seconds.";
    }

    void GameController::shutdownGame() {
        // TODO: Stub
        is_game_running = false;
    }

    GameController::GameController(std::vector<Area> a) {
        // TODO: Default area model/controller should be added here?
        areas.reserve(a.size());
        for (auto &area : a) {
            areas.emplace(area.getName(), area);
        }
    }

    /*
     * Method for server to register a player login with the Game.
     */
    void GameController::playerJoinArea(const GameDataWrapper &g) {
        auto request = g.data.get<JoinArea>();
        if (areas.find(request.area) != areas.end()) {
            playerToArea.emplace(g.connection_identifier, request.area);
            areas.at(request.area).receiveIncoming(g);
        } else {
            // TODO: Handle case where player trys to join non-existant area
            LOG(WARNING) << "Received Join request for invalid Area name: " << request.area << g;
        }
    }
}
