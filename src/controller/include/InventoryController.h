#ifndef ADVENTURE2019_INVENTORYCONTROLLER_H
#define ADVENTURE2019_INVENTORYCONTROLLER_H

#include "Controller.h"
#include "Parser.h"
#include "StructModels.h"
#include "Object.h"
#include "Inventory.h"
#include "ID.h"
#include "Item.h"

#include <unordered_map>

#include <iostream>
#include <string>
#include <unordered_map>

//make some change to push
namespace adventure::controller {
    using adventure::model::Object;
    using adventure::model::Inventory;
    using adventure::model::GameDataWrapper;
    using adventure::model::IdHasher;
    using adventure::model::Id;
    using adventure::model::InventoryCommand;
    using adventure::model::Item;
    using Equipment = std::optional<Item>;

    class InventoryController {
    public:
        InventoryController() = default;
        InventoryController(const std::vector<Object> &objs);

        InventoryId createInventory();
        const Item &getItem(ItemId itemId, InventoryId inventoryId) const;
        const Item &getItem(ItemId itemId) const;
        bool isItemInInventory(ItemId itemId, InventoryId inventoryId) const;
        void moveItem(ItemId itemId, InventoryId oldInventoryId, InventoryId newInventoryId);

        // Does not handle containers -> use moveItem() instead
        void addItem(const Item &item, InventoryId inventoryId);
        Item removeItem(ItemId itemId, InventoryId inventoryId);



        //should be used to implement getters for object info  like getDesctioption, etc
        const Object &getObjectByItemId(ItemId itemId, InventoryId inventoryId) const;

        const Object &getObject(ObjectType type) const;


            bool isObjectInInventory(ObjectType objectType, InventoryId inventoryId) const;

        const Object &getFirstObjectByKeyword(std::string_view keyword)const;

        ItemId getItemIdByKeyword(std::string_view keyword, InventoryId inventoryId) const;

        bool isObjectKeywordInInventory(std::string_view keyword, InventoryId inventoryId) const;

        bool isObjectKeywordExist(std::string_view keyword) const;

        ObjectType getObjectTypeByKeyword(std::string_view keyword) const;

        const Item &getItemInInventory(ItemId itemId, InventoryId inventoryId) const;

        ItemId addNewItem(ObjectType type, InventoryId inventoryId);

        const Inventory &getInventory(InventoryId inventoryId) const;

        const std::vector<Item> &getItemsInInventory(InventoryId inventoryId) const;

        void addNewItemToContainer(ObjectType type, ItemId container);

        void putItemInContainer(ItemId container, ItemId item, InventoryId inventoryId);

        std::vector<ObjectType> getAllObjectTypesByKeyword(std::string_view keyword, InventoryId inventoryId);

        void equipItem(const Item &item, SlotLocation slot, CharId charId);

        Item unequipItem(SlotLocation slot, CharId charId);

        bool isSlotEmpty(SlotLocation slot, CharId charId);

    private:
        long long inv_id{1};
        std::unordered_map<InventoryId, Inventory, StrongAliasIdHasher> inventoryMap;
        std::unordered_map<CharId, std::vector<Equipment>, StrongAliasIdHasher> equipMap;

        ResponseMap outgoing;

        std::unordered_map<ObjectType, Object, StrongAliasIdHasher> objectMap;

        Item &getMutableItem(ItemId itemId, InventoryId inventoryId);
    };
}
#endif //ADVENTURE2019_INVENTORYCONTROLLER_H
