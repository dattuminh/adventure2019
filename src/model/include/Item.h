#ifndef ADVENTURE2019_ITEM_H
#define ADVENTURE2019_ITEM_H

#include "ID.h"

#include <optional>
#include <cassert>

namespace adventure::model {

    class Container{
    public:
        Container() = default;
        Container(std::vector<ItemId> ids) : objs{ids} {}
        bool exist() const{
            return objs.has_value();
        }

        bool itemExist(ItemId itemId) const{
            return std::find(objs->begin(), objs->end(), itemId) != objs->end();
        }

        void addObject(ItemId itemId){
            objs->emplace_back(itemId);
        }

        void removeObject(ItemId itemId){
            auto itr = std::find(objs->begin(), objs->end(), itemId);
            objs->erase(itr);
        }
        void setContainer(){
            objs = std::vector<ItemId>();
        }

        std::vector<ItemId> getAllItemIds(){
            return objs.value();
        }

        size_t size() const {return objs->size();}

    private:
        std::optional<std::vector<ItemId>> objs;
    };

    /**
     * Represents an Instance of an @class Object.
     * It is a program error to access the contents of an Item if it is not a container.
     */
    class Item {
    public:
        Item() = default;

        ~Item() = default;

        bool isContainer() const { return contents.exist(); }

        void setContents(std::vector<ItemId> c){ contents = Container{c}; }

        const Container &getContents() const {
            assert(isContainer());
            return contents;
        }

        ObjectType getType() const { return type; }

        void setId(ItemId i) { id = i; }

        ItemId getId() const { return id; }

        size_t getContentSize() const {
            assert(isContainer());
            return contents.size();
        }

        void addObjectToContainer(ItemId itemId){
            contents.addObject(itemId);
        }

        void removeObjectToContainer(ItemId itemId){
            contents.removeObject(itemId);
        }

        bool itemExistInContainer(ItemId itemId){
            return contents.itemExist(itemId);
        }

        Container &getMutableContainer(){ return contents;}

    private:
        ItemId id{};
        ObjectType type;
        Container contents;
    };
}
#endif //ADVENTURE2019_ITEM_H
