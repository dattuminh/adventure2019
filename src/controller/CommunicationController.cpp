#include "CommunicationController.h"

using namespace adventure::controller;

void CommunicationController::handleCommCommand(const CCommand<ChatCommand> &command) {
    switch (command.data.type) {
        case CommCommands::says: {
            handleSay(command);
            break;
        }
        case CommCommands::yell: {
            handleYell(command);
            break;
        }
        case CommCommands::tell: {
            handleTell(command);
            break;
        }
        default: {
            LOG(ERROR) << "Not implemented handleCommCommand() " << command.data.type;
        }
    }
}

void CommunicationController::handleSay(const CCommand<ChatCommand> &command) {
    CCommand<ChatMessage> cm{.id = command.id, .data = {.from = {}, .text = command.data.text}};
    handleChat(cm);
}

void CommunicationController::handleYell(const CCommand<ChatCommand> &command) {
    auto location = charController->getCharacterLocation(command.id);
    auto source_room = roomController->getRoom(location);
    ChatMessage msg{.from = charController->getCharacterName(command.id), .text = "** " + command.data.text + "!!"};

    sendData(source_room.getCharacters(), msg);

    for (const auto &door : source_room.getDoors()) {
        auto room = roomController->getRoom(door.getTo());
        sendData(room.getCharacters(), msg);
    }
}

void CommunicationController::handleChat(CCommand<ChatMessage> &cm) {
    auto location = charController->getCharacterLocation(cm.id);
    auto room = roomController->getRoom(location);
    cm.data.from = charController->getCharacterName(cm.id);
    sendData(room.getCharacters(), cm.data);
}

void CommunicationController::handleTell(const CCommand<ChatCommand> &command) {
    auto room_id = charController->getCharacterLocation(command.id);
    auto room = roomController->getRoom(room_id);
    auto keywords = charController->getKeywords(room.getCharacters());
    auto receiver = command.data.to.value();

    for (const auto &keyword : keywords) {
        if (keyword.second == receiver) {
            ChatMessage m{.from = charController->getCharacterName(command.id), .text = command.data.text};
            sendData(keyword.first, m);
            sendData(command.id, m);
            return;
        }
    }
    // Case where receiver was not found
    ChatMessage m{.from = "Server", .text = "Could not find " + receiver};
    sendData(command.id, m);
}

void CommunicationController::sendData(CharId receiver, json msg) {
    outgoing->operator[](receiver).emplace_back(msg);
}

void CommunicationController::sendData(const std::vector<CharId> &receivers, json msg) {
    for (const auto &receiver : receivers) {
        outgoing->operator[](receiver).emplace_back(msg);
    }
}
