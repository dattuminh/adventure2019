#include "LookController.h"

namespace adventure::controller {
    void LookController::handleLookCommand(Command command) {
        auto userId = playerController->getActiveAvatar(command.sender);

        auto roomId = charController.lock()->getCharacterLocation(userId);
        auto name = roomController->getRoomName(roomId);
        auto desc = roomController->getRoomDescription(roomId);

        auto doors = roomController->getRoom(roomId).getDoors();

        auto output = name + "\n" + desc + "\n";
        std::string doorsDescriptions = "Doors: \n";
        if (doors.empty()) {

            doorsDescriptions.append(" None\n");
        } else {
            for (auto door:doors) {

                doorsDescriptions.append(door.getDirection()._to_string());
                doorsDescriptions.append(", State: ");
                //doorsDescriptions.append(door.getState()._to_string());
                doorsDescriptions.append(", Description: ");
                doorsDescriptions.append(door.getDescription());
                doorsDescriptions.append("\n");
            }
        }
        output.append(doorsDescriptions);


        ChatMessage msg{"Server", output};
        json j;
        to_json(j, msg);
        GameDataWrapper g{command.sender, j};
        outgoing.lock()->emplace_back(g);

    }
}