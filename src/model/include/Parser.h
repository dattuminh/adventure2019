#ifndef ADVENTURE2019_PARSER_H
#define ADVENTURE2019_PARSER_H

#include "Adventure.h"
#include "Area.h"
#include "Door.h"
#include "Extra.h"
#include "Item.h"
#include "NPC.h"
#include "Object.h"
#include "Reset.h"
#include "Room.h"
#include "StructModels.h"

#include <glog/logging.h>
#include <nlohmann/json.hpp>
#include <boost/filesystem.hpp>
#include <vector>
#include <utility>
#include <sstream>
#include <string>
#include <type_traits>

using json = nlohmann::json;
namespace fs = boost::filesystem;

namespace adventure {
    constexpr int const DEBUG_JSON_INDENT = 4;
}

/**
 * JSON Model Parsing Implementation
 *
 * Common types:
 * ID: Almost all JSON types have an positive integer identifier. (AFAIK)
 * Since it must be positive, throw when it isn't. Use the get_id() helper method and pass in the model name for
 * debugging purposes. If it not found, the json library will throw an exception.
 *
 * Dir: Any type with a Dir field must have a valid Dir.
 *
 * To: Same as ID but specific to Doors.
 *
 */

namespace adventure::model {
    std::vector<Area> loadAreasFromFiles(const fs::path &p);
    void parseAreaJson(const fs::path &p, std::vector<Area> &areas);
    Action getAction(const json &j);
    template<typename T,
             typename = std::enable_if<std::is_default_constructible<T>::value>>
    inline std::vector<T> get_vector(const json &j, const char *property);
    inline std::string get_string(const json &j, const char *property);
    template<typename T>
    inline T getModelFromModelData(const ModelData &d);


    // from_json
    inline void from_json(const json &j, Area &a);
    inline void from_json(const json &j, Door &d);
//    inline void from_json(const json &j, Helps &h);
    inline void from_json(const json &j, NPC &c);
    inline void from_json(const json &j, Object &o);
    inline void from_json(const json &j, Room &r);
    inline void from_json(const json &j, Reset &r);
//    inline void from_json(const json &j, Shop &s);
    inline void from_json(const json &j, Extra &x);
    inline void from_json(const json &j, Id &i);
    inline void from_json(const json &j, ChatMessage &c);
    inline void from_json(const json &j, GenericCommand &c);
    inline void from_json(const json &j, Move &m);
    inline void from_json(const json &j, Command &c);
    inline void from_json(const json &j, Debug &d);
    inline void from_json(const json &j, GameDataWrapper &g);
    inline void from_json(const json &j, ModelData &m);
    inline void from_json(const json &j, ChatCommand &c);
    inline void from_json(const json &j, InventoryCommand &i);
    inline void from_json(const json &j, ServerStatus &s);
    inline void from_json(const json &j, LookCommand &l);
    inline void from_json(const json &j, ErrorMessage &e);
    inline void from_json(const json &j, AuthResponse &a);

    // to_json
    inline void to_json(json &j, const Area &a);
    inline void to_json(json &j, const Door &d);
//    inline void to_json(json &j, const Helps &h);
//    inline void to_json(json &j, const NPC &n);
    inline void to_json(json &j, const Object &x);
    inline void to_json(json &j, const Room &x);
//    inline void to_json(json &j, const Shop &s);
    inline void to_json(json &j, const Extra &x);
    inline void to_json(json &j, const Id &i);
    inline void to_json(json &j, const ChatMessage &c);
    inline void to_json(json &j, const GenericCommand &c);
    inline void to_json(json &j, const Move &m);
    inline void to_json(json &j, const Command &c);
    inline void to_json(json &j, const Debug &d);
    inline void to_json(json &j, const GameDataWrapper &g);
    inline void to_json(json &j, const ModelData &m);
    inline void to_json(json &j, const ChatCommand &c);
    inline void to_json(json &j, const InventoryCommand &i);
    inline void to_json(json &j, const ServerStatus &s);
    inline void to_json(json &j, const LookCommand &l);
    inline void to_json(json &j, const ErrorMessage &e);
    inline void to_json(json &j, const AuthResponse &e);

    template<typename Tag>
    inline void to_json(json &j, const StrongAlias<Id, Tag> &id);

    /*
     * Default field_name is "id", but in some cases it isn't such as the Room ID in Reset.
     */
    inline Id get_id(const json &j, const std::string &model_name, const std::string &field_name = "id") {
        auto id = j.find(field_name);
        if (id == j.end()) {
            throw std::runtime_error("ID for model " + model_name + " was not found");
        }
        Id ret{id->get<long long>()};
        if (ret.id < 0) {
            throw std::underflow_error(model_name + " was not positive");
        }
        return ret;
    }

    template<typename Tag>
    inline StrongAlias<Id, Tag>
    get_strong_id(const json &j, const std::string &model_name, const std::string &field_name = "id") {
        return StrongAlias<Id, Tag>{get_id(j, model_name, field_name)};
    }

    template<typename T, typename>
    inline std::vector<T> get_vector(const json &j, const char *property) {
        if (j.find(property) != j.end()) {
            return j.at(property).get<std::vector<T>>();
        }
        return std::vector<T>();
    }

    inline std::string get_string(const json &j, const char *property) {
        if (j.find(property) != j.end()) {
            return j.at(property).get<std::string>();
        }
        LOG(WARNING) << "The string for property " << property << " was not found";
        return "";
    }

    inline std::string get_concat_string(const json &j, const char *property) {
        std::ostringstream result;
        for (const auto &s : get_vector<std::string>(j, property)) {
            result << s << " ";
        }

        return result.str();
    }

    // This will throw if not found
    template<typename T>
    inline T get_enum(const json &j, const char *property) {
        return T::_from_string_nocase(j.at(property).get<std::string>().c_str());
    }

    inline void from_json(const json &j, Area &a) {
        a.setName(j.at("AREA").at("name").get<std::string>());
//        a.setHelps(j.at("HELPS").get<std::vector<json>>());
        a.setNpcs(j.at("NPCS").get<std::vector<NPC>>());
        a.setObjects(j.at("OBJECTS").get<std::vector<Object>>());
        a.setRooms(j.at("ROOMS").get<std::vector<Room>>());
        a.setResets(j["RESETS"].get<std::vector<Reset>>());
        a.setShops(j.at("SHOPS").get<std::vector<json>>());
    }

    inline void from_json(const json &j, Extra &x) {
        x.setKeywords(get_vector<std::string>(j, "keywords"));
        x.setDescription(get_concat_string(j, "desc"));
    }

    inline void from_json(const json &j, Object &o) {
        o.setId(get_strong_id<ObjectTypeTag>(j, "Object"));
        o.setKeywords(get_vector<std::string>(j, "keywords"));
        o.setShortDesc(get_string(j, "shortdesc"));
        o.setLongDesc(get_vector<std::string>(j, "longdesc"));
        o.setExtra(get_vector<Extra>(j, "extra"));
    }

    inline void from_json(const json &j, Reset &r) {
        r.setId(get_id(j, "Reset"));
        r.setAction(get_enum<Reset::Action>(j, "action"));

        if (j.find("limit") != j.end()) { r.setLimit(j.at("limit")); }
        if (j.find("room") != j.end()) {
            r.setRoom(get_strong_id<RoomIdTag>(j, "Reset", "room"));
        }
        if (j.find("slot") != j.end()) { r.setSlot(j.at("slot")); }
        if (j.find("state") != j.end()) {
            r.setState(DoorState::_from_string_nocase_nothrow(j.at("state").get<std::string>().c_str()));
        }
        if (j.find("container") != j.end()) { r.setContainer(get_id(j, "reset", "container")); }
    }

    inline void from_json(const json &j, Room &r) {
        r.setId(get_strong_id<RoomIdTag>(j, "Room"));
        r.setName(model::get_string(j, "name"));
        r.setDesc(get_concat_string(j, "desc"));
        r.setDoors(model::get_vector<Door>(j, "doors"));
        r.setExtendedDescriptions(model::get_vector<Extra>(j, "extended_descriptions"));
    }

    inline void to_json(json &j, const Extra &x) {
        j["keywords"] = x.getKeywords();
        j["desc"] = json{x.getDescription()};
    }

    inline void to_json(json &j, const Object &x) {
        j["id"] = x.getId();
        j["keywords"] = x.getKeywords();
        j["shortdesc"] = x.getShortDesc();
        j["longdesc"] = x.getLongDesc();
        j["extra"] = x.getExtra();
    }

    inline void to_json(json &j, const Room &x) {
        j["id"] = x.getId();
        j["name"] = x.getName();
        j["desc"] = json{x.getDesc()};
        j["doors"] = x.getDoors();
        j["extended_descriptions"] = x.getExtendedDescriptions();
    }

    inline void from_json(const json &j, Door &d) {
        d.setTo(get_strong_id<RoomIdTag>(j, "Door", "to"));
        d.setDirection(get_enum<Direction>(j, "dir"));
        d.setDescription(get_concat_string(j, "desc"));
        d.setKeywords(get_vector<std::string>(j, "keywords"));
    }

    inline void to_json(json &j, const Door &d) {
        if (d.getState()) { j["state"] = (*d.getState())._to_string(); }
        j["dir"] = d.getDirection();
        j["desc"] = d.getDescription();
        j["keywords"] = d.getKeywords();
        j["to"] = d.getTo();
    }

    inline void to_json(json &j, const ServerStatus &s) {
        j = {
                {"player_count", s.player_count},
                {"max",          s.max_players},
                {"connected",    s.connected_players},
                {"areas",        s.areas},
                {"Action",       s.action._to_string()}
        };
    }

    inline void from_json(const json &j, ServerStatus &s) {
        s.player_count = j.at("player_count").get<unsigned long>();
        s.max_players = j.at("max").get<long>();
        s.connected_players = get_vector<std::string>(j, "connected");
        s.areas = get_vector<std::string>(j, "areas");
    }

    inline void from_json(const json &j, NPC &c) {
        c.setId(get_strong_id<NPCIdTag>(j, "NPC"));
        c.setKeywords(get_vector<std::string>(j, "keywords"));
        c.setShortDesc(get_string(j, "shortdesc"));
        c.setLongDesc(get_vector<std::string>(j, "longdesc"));
        c.setDescription(get_concat_string(j, "description"));
    }

    /* This shouldn't be needed since get_id() should be called.
    inline void from_json(const json &j, Id &i) {
        i.id = j.get<long long>();
    }
     */

    inline void to_json(json &j, const Id &i) {
        j["id"] = i.id;
    }

    inline void from_json(const json &j, Command &c) {
        c.action = get_enum<Action>(j, "Action");
        c.input = get_string(j, "input");
        c.command = get_string(j, "command");
    }

    inline void to_json(json &j, const Command &c) {
        j["Action"] = c.action._to_string();
        j["input"] = c.input;
        j["command"] = c.command;
    }

    inline void to_json(json &j, const ChatMessage &c) {
        j["from"] = c.from;
        j["text"] = c.text;
        j["Action"] = ChatMessage::action._to_string();
    }

    inline void from_json(const json &j, ChatMessage &c) {
        c.from = get_string(j, "from");
        c.text = get_string(j, "text");
    }

    inline void from_json(const json &j, Debug &d) {
        d.debugCmd = get_enum<DebugCmd>(j, "DebugCmd");
    }

    inline void to_json(json &j, const Debug &d) {
        j["Action"] = adventure::model::Debug::action._to_string();
        j["DebugCmd"] = d.debugCmd._to_string();
    }

    inline void from_json(const json &j, GameDataWrapper &g) {
        g.connection_identifier = get_string(j, "connection_identifier");
        g.data = get_string(j, "data");
    }

    inline void to_json(json &j, const GameDataWrapper &g) {
        j["connection_identifier"] = g.connection_identifier;
        j["data"] = g.data;
    }

    inline void from_json(const json &j, JoinArea &ja) {
        ja.area = j.at("Area").get<std::string>();
    }

    inline void to_json(json &j, const JoinArea &ja) {
        j["Action"] = JoinArea::action._to_string();
        j["Area"] = ja.area;
    }

    inline void from_json(const json &j, Authenticate &a) {
        a.username = get_string(j, "username");
    }

    inline void to_json(json &j, const Authenticate &a) {
        j["Action"] = Authenticate::action._to_string();
        j["username"] = a.username;
    }

    inline void to_json(json &j, const ModelData &m) {
        j[m.type._name()] = ModelData::action._to_string();
        j["type"] = m.type._to_string();
        j["data"] = m.data;
        j["Action"] = ModelData::action._to_string();
    }

    inline void from_json(const json &j, ModelData &m) {
        m.type = get_enum<ModelType>(j, "type");
        m.data = j.at("data");
    }

    /**
     * Convert a Model into JSON for sending to Clients
     * @tparam T The type of model
     * @param model The model you want to convert
     * @param type the type of Model you are sending
     * @return the JSON representation of the model
     */
    template<typename T>
    inline json toJson(const T &model, ModelType type) {
        ModelData m;
        m.data = model;
        m.type = type;
        return m;
    }

    inline void from_json(const json &j, Move &m) {
        m.direction = get_enum<Direction>(j, "direction");
    }

    inline void to_json(json &j, const Move &m) {
        j["Action"] = adventure::model::Move::action._to_string();
        j["direction"] = m.direction._to_string();
    }

    inline void from_json(const json &j, ChatCommand &c) {
        c.type = get_enum<CommCommands>(j, "type");
        c.text = j.at("text");
        if (j.find("to") != j.end()) { c.to = j.at("to"); }
    }

    inline void to_json(json &j, const ChatCommand &c) {
        j["Action"] = adventure::model::ChatCommand::action._to_string();
        j["text"] = c.text;
        j["type"] = c.type;
        if (c.to.has_value()) { j["to"] = c.to.value(); }
    }

    inline void from_json(const json &j, InventoryCommand &i){
        i.type = get_enum<InventoryAction>(j, "type");
        i.item = get_string(j, "item");
        i.container = get_string(j, "container");
        i.giveTo = get_string(j, "giveTo");
        i.slot = get_enum<SlotLocation>(j, "slot");
    }

    inline void to_json(json &j, const InventoryCommand &i) {
        j["Action"] = i.action._to_string();
        j["type"] = i.type;
        j["item"] = i.item;
        j["container"] = i.container;
        j["giveTo"] = i.giveTo;
        j["slot"] = i.slot._to_string();
    }

    inline void from_json(const json &j, LookCommand &l) {
        l.type = get_enum<LookAction>(j, "type");
        l.arg = get_string(j, "arg");
    }

    inline void to_json(json &j, const LookCommand &l) {
        j["Action"] = LookCommand::action._to_string();
        j["arg"] = l.arg;
        j["type"] = l.type._to_string();
    }

    inline void from_json(const json &j, ErrorMessage &e) {
        e.type = get_enum<ModelType>(j, "type");
        e.message = get_string(j, "message");
    }

    inline void to_json(json &j, const ErrorMessage &e) {
        j["Action"] = ErrorMessage::action._to_string();
        j["type"] = e.type._to_string();
        j["message"] = e.message;
    }

    template <class Model>
    inline void from_json(const json &j, Confirmation<Model> &c) {
        c.options = get_vector<Model>(j, "options");
        c.type = get_enum<ModelType>(j, "type");
    }

    template <class Model>
    inline void to_json(json &j, const Confirmation<Model> &c) {
        j["Action"] = c.action._to_string();
        j["type"] = c.type._to_string();
        json options;
        for(const auto &model : c.options){
            options.push_back(model);
        }
        j["options"] = options;
    }

    inline void from_json(const json &j, AuthResponse &a) {
        a.authenticated = j.at("authenticated").get<bool>();
        a.text = get_string(j, "message");
        a.action = get_enum<Action>(j, "Action");
    }

    inline void to_json(json &j, const AuthResponse &a) {
        j["Action"] = a.action._to_string();
        j["authenticated"] = a.authenticated;
        j["message"] = a.text;
    }

    template<typename Tag>
    inline void to_json(json &j, const StrongAlias<Id, Tag> &id) {
        j = id.value;
    }
}

#endif