#ifndef ADVENTURE2019_INVENTORY_H
#define ADVENTURE2019_INVENTORY_H

#include "Object.h"
#include "ID.h"
#include "Item.h"
#include <iostream>
#include <string>
#include <vector>

namespace adventure {
    namespace model {
        class Inventory {
        private:
            std::vector<Item> inventory;

            std::vector<adventure::model::Item, std::allocator<adventure::model::Item>>::const_iterator
            findItemItr(ItemId itemId) const {
                return std::find_if(inventory.begin(), inventory.end(), [&](const Item &item){
                    return item.getId() == itemId;
                });
            }
        public:


            void addItem(const Item &item) {
                inventory.emplace_back(item);
            }

            const Item &getItem(ItemId itemId) const{
                return *findItemItr(itemId);
            }

            Item &getMutableItem(ItemId itemId) {
                return std::find_if(inventory.begin(), inventory.end(), [&](const Item &item){
                    return item.getId() == itemId;
                }).operator*();
            }

            Item removeItem(ItemId itemId) {
                auto itemItr = std::find_if(inventory.begin(), inventory.end(), [&](const Item &item){
                    return item.getId() == itemId;
                });
                auto item = *itemItr;
                inventory.erase(itemItr);
                return item;
            }

            int getSize(){
                return inventory.size();
            }

            bool itemInInventory(ItemId itemId) const {
                return findItemItr(itemId) != inventory.end();
            }

            ObjectType getObjectType(ItemId itemId) const{
                auto itr = findItemItr(itemId);
                return itr->getType();
            }

            bool objectInInventory(ObjectType objectType) const{
                auto itr = std::find_if(inventory.begin(), inventory.end(), [&](const Item &item){
                    return item.getType() == objectType;
                });
                return itr != inventory.end();
            }

            const Item &getItem(ObjectType type) const{
                auto itr = std::find_if(inventory.begin(), inventory.end(), [&](const Item &item){
                    return item.getType() == type;
                });
                return *itr;
            }

        };
    }
}

#endif //ADVENTURE2019_INVENTORY_H
