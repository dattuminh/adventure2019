#ifndef ADVENTURE2019_ROOMCONTROLLER_H
#define ADVENTURE2019_ROOMCONTROLLER_H

#include "Controller.h"
#include "RoomController.h"
#include "Parser.h"
#include "Room.h"
#include "Object.h"
#include "StructModels.h"
#include "CharacterController.h"
#include "PlayerController.h"
#include "InventoryController.h"

#include <string>
#include <unordered_map>

namespace adventure::controller {

    using adventure::model::Door;
    using adventure::model::IdHasher;
    using adventure::model::Id;
    using adventure::model::Room;
    using adventure::model::Player;
    using adventure::model::ChatMessage;

    class RoomController {
    public:
        /**
         * Move each room from vector rooms into the map, using the Id as the key.
         * @param rooms The Rooms to control
         * @param u CharacterController
         * @param out The Outgoing Message Map
         */
        RoomController(std::vector<Room> room_models,
                       std::weak_ptr<CharacterController> u,
                       std::weak_ptr<InventoryController> invC,
                       ResponseMap out);

        ~RoomController() = default;

        /* Mutators */

        /**
         * Adds a Character to a room
         *
         * @param Character object that will be moved
         * @param roomID of the room
         */
        void addCharacterToRoom(CharId char_id, RoomId roomID);

        /**
         * @throws std::runtime_error if Character is not in Room.
         * @param char_id
         * @param roomID
         */
        void removeCharacterFromRoom(CharId char_id, RoomId roomID);

        /**
         * @throws std::runtime_error when trying to move a Character not in origin room
         * @param char_id
         * @param origin_room
         * @param dest_room
         */
        void addAndRemoveCharacter(CharId char_id, RoomId origin_room, RoomId dest_room);

        /* Const Read Only Accessors */

        /**
         * Returns the room object with the same roomID
         *
         * @throws std::runtime_error when accessing non-existant room
         * @param roomID
         * @return const Room object reference
         */
        const Room &getRoom(RoomId roomID) const;

        /**
         * @throws std::runtime_error when accessing non-existant room
         * @param roomId
         * @return
         */
        std::string getRoomName(RoomId roomId) const;

        /**
         * @throws std::runtime_error when accessing non-existant room
         * @param roomId
         * @return
         */
        std::string getRoomDescription(RoomId roomId) const;

        /**
         * Check if a Room ID exists
         * @param roomId
         * @return
         */
        bool roomExists(RoomId roomId) const;

        /**
         * Performs checks on the models, and removes invalid data.
         */
        void checkRooms();

        InventoryId getRoomInventoryId(RoomId roomId) const;

    private:
        std::weak_ptr<CharacterController> charCtrl;
        std::weak_ptr<InventoryController> inventoryCtrl;
        ResponseMap outgoing;
        std::unordered_map<RoomId, Room, StrongAliasIdHasher> roomMap;

        void addOutgoingData(CharId player_id, std::string data) const;

        Room &getMutableRoom(RoomId roomID);

        void sendRoomData(CharId player_id, RoomId room_id) const;
    };
}

#endif //ADVENTURE2019_ROOMCONTROLLER_H
