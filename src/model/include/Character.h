#ifndef CMPT373_PROJECT_CHARACTER_H
#define CMPT373_PROJECT_CHARACTER_H

#include "Adventure.h"
#include "StructModels.h"
#include "Inventory.h"
#include "Object.h"
#include "ID.h"
#include "NPC.h"

#include <iostream>
#include <string>
#include <utility>
#include <vector>

namespace adventure::model {

    /*
     * Models an entity in the game that is playable. For example the Player Avatar or an NPC.
     */
    class Character {
    public:
        Character() = default;

        Character(std::string n, long long i) : id{i}, name{std::move(n)} {}

        Character(std::string n, CharId i, InventoryId inventoryId) : id{i}, inventory{inventoryId}, name{std::move(n)} {}

        Character(const NPC &npc, CharId i, RoomId l) : id{i},
                                                        location{l},
                                                        name{npc.getShortDesc()},
                                                        shortdesc{npc.getShortDesc()},
                                                        longdesc{npc.getLongDesc()},
                                                        description{npc.getDescription()},
                                                        keywords{npc.getKeywords()} {}

        ~Character() = default;

        const std::string &getName() const { return name; }

        void setName(std::string n) { name = std::move(n); }

        CharId getId() const { return id; }

        void setId(CharId i) { id = i; }

        void changeHealth(int hp){
            if(hp < 0 && health.value - abs(hp) >= 0){
                health.value -= abs(hp);
            }
            if(hp > 0 && health.value + abs(hp) <= 100){
                health.value += abs(hp);
            }
        }

        HP getHealth() const{ return health; }

        void setShortDesc(std::string shortdesc){ this->shortdesc = shortdesc; }

        const std::string getShortDesc() const { return shortdesc; }

        void setLongDesc(std::vector<std::string> longdesc){ this->longdesc = longdesc; }

        const std::vector<std::string> getLongDesc() const { return longdesc; }

        void setDescription(std::string description){ this->description = description; }

        const std::string getDescription() const { return description; }

        void setKeywords(std::vector<std::string> keywords) { this->keywords = keywords; }

        RoomId getLocation() const { return location; };

        void setLocation(RoomId l) { location = l; }

        InventoryId getInventory() const { return inventory; }

        void setInventory(InventoryId i) { inventory = i; }

        const std::vector<std::string> &getKeywords() const { return keywords; }

    private:
        CharId id{};
        RoomId location{};
        InventoryId inventory{};
        HP health{100};
        MP mana{};
        std::string name{};
        std::string shortdesc{};
        std::vector<std::string> longdesc{};
        std::string description{};
        std::vector<std::string> keywords{};
    };
}

#endif //CMPT373_PROJECT_CHARACTER_H
