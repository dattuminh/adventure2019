#include "Room.h"
#include "Parser.h"
#include "MovementController.h"

namespace adventure::controller {
    MovementController::MovementController(std::weak_ptr<RoomController> room,
                                           std::weak_ptr<CharacterController> character,
                                           RoomId def_room,
                                           ResponseMap out) :
            roomCtrl{std::move(room)},
            charCtrl{std::move(character)},
            defaultRoom{def_room},
            outgoing{std::move(out)} {}

    void MovementController::handleMoveCommand(const CCommand<model::Move> &command) const {
        auto requestedDirection = command.data.direction;
        auto origin_room = roomCtrl.lock()->getRoom(charCtrl.lock()->getCharacterLocation(command.id));

        if (origin_room.hasDirection(requestedDirection)) {
            if (auto door = origin_room.getDoor(requestedDirection); doorIsOpen(command.id, door)) {
                auto destinationRoom = door.getTo();
                roomCtrl.lock()->addAndRemoveCharacter(command.id, origin_room.getId(), destinationRoom);
                charCtrl.lock()->setCharacterLocation(command.id, destinationRoom);
            }
        } else {
            std::string msg = "Room has no door to ";
            addOutgoingChat(command.id, msg + requestedDirection._to_string());
        }
    }

    void MovementController::addOutgoingChat(CharId receiver, const std::string &message) const {
        ChatMessage c{.from = "Server", // TODO: Need to agree on what this should be for server events.
                .text = message
        };
        outgoing->operator[](receiver).emplace_back(c);
    }

    void MovementController::spawnCharacter(CharId character_id) const {
        roomCtrl.lock()->addCharacterToRoom(character_id, defaultRoom);
        charCtrl.lock()->setCharacterLocation(character_id, defaultRoom);
    }

    bool MovementController::doorIsOpen(CharId id, const Door &door) const {
        auto state = door.getState();
        if (state) {
            switch (*state) {
                case model::DoorState::Closed: {
                    addOutgoingChat(id, "You can't go through something that is closed!");
                    break;
                }
                case model::DoorState::Locked: {
                    addOutgoingChat(id, "You can't go that way, it is locked.");
                    break;
                }
                case model::DoorState::Open: {
                    break;
                }
                default: {
                    LOG(ERROR) << "Reached impossible case! State: " << *state;
                }
            }
        }

        return !state || *state == +model::DoorState::Open;
    }
}
