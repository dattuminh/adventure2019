#ifndef ADVENTURE2019_COMMUNICATIONCONTROLLER_H
#define ADVENTURE2019_COMMUNICATIONCONTROLLER_H

#include "Controller.h"
#include "Adventure.h"
#include "CharacterController.h"
#include "RoomController.h"

#include <string>
#include <memory>
#include <utility>

namespace adventure::controller {

    class CommunicationController {
    public:
        CommunicationController(std::shared_ptr<CharacterController> userC,
                                std::shared_ptr<RoomController> roomC,
                                ResponseMap out) :
                charController{std::move(userC)}, roomController{std::move(roomC)}, outgoing{std::move(out)} {};

        ~CommunicationController() = default;

        void handleCommCommand(const CCommand<ChatCommand> &command);

        void handleChat(CCommand<ChatMessage> &cm);

    private:
        std::shared_ptr<CharacterController> charController;
        std::shared_ptr<RoomController> roomController;
        ResponseMap outgoing;

        void sendData(CharId receiver, json msg);

        void sendData(const std::vector<CharId> &receivers, json msg);

        void handleSay(const CCommand<ChatCommand> &command);

        void handleYell(const CCommand<ChatCommand> &command);

        void handleTell(const CCommand<ChatCommand> &command);
    };

}
#endif //ADVENTURE2019_COMMUNICATIONCONTROLLER_H
