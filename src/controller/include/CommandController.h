#ifndef ADVENTURE2019_COMMANDCONTROLLER_H
#define ADVENTURE2019_COMMANDCONTROLLER_H

#include "Controller.h"
#include "Adventure.h"
#include "CommunicationController.h"
#include "LookController.h"
#include "Player.h"
#include "MovementController.h"
#include "InvCommandController.h"

#include <nlohmann/json.hpp>
#include <string>
#include <utility>

namespace adventure::controller {

    class CommandController {
    public:
        CommandController(std::shared_ptr<LookController> lookC,
                          std::shared_ptr<CommunicationController> commC,
                          std::shared_ptr<MovementController> moveC,
                          std::weak_ptr<PlayerController> playerC,
                          std::shared_ptr<InvCommandController> invC,
                          ResponseDataList out) : commController{std::move(commC)},
                                                  lookController{std::move(lookC)},
                                                  moveController{std::move(moveC)},
                                                  playerController{std::move(playerC)},
                                                  invController{std::move(invC)},
                                                  outgoing{std::move(out)} {};
        ~CommandController() = default;

        void handleCommand(const GameDataWrapper &g);

        template<typename StructModel>
        CCommand<StructModel> toCommand(const GameDataWrapper &g);

    private:
        std::shared_ptr<CommunicationController> commController;
        std::shared_ptr<LookController> lookController;
        std::shared_ptr<MovementController> moveController;
        std::weak_ptr<PlayerController> playerController;
        std::shared_ptr<InvCommandController> invController;
        ResponseDataList outgoing;
        GameDataWrapper
        generateResponse(const std::string &from, const std::string &identifier, const std::string &response);
    };

}

#endif
