#ifndef ADVENTURE2019_USER_H
#define ADVENTURE2019_USER_H

#include "ID.h"
#include "StructModels.h"
#include "Character.h"

#include <string>
#include <vector>

namespace adventure::model {

    class Player {
    public:
        Player() = default;

        Player(std::string u, CharId id) {
            this->username = u;
            avatars.emplace_back(id);
        }

        std::string getUsername() const { return username; }

        CharId getActiveAvatar() const { return avatars.front(); } // TODO: Implement properly

    private:
        std::vector<CharId> avatars{};
        std::string username;
    };
}

#endif //CMPT373_USER_H
