#include "RoomController.h"

namespace adventure::controller {

    void RoomController::addCharacterToRoom(CharId char_id, RoomId roomID) {
        getMutableRoom(roomID).addCharacter(char_id);
        sendRoomData(char_id, roomID);
        // TODO alert other players in room
        LOG(INFO) << "Player " << char_id << " added to Room ID: " << roomID;
    }

    void RoomController::removeCharacterFromRoom(CharId char_id, RoomId roomID) {
        // TODO: Alert other players in room

        if (auto room = getMutableRoom(roomID); room.hasPlayer(char_id)) {
            room.removeCharacter(char_id);
            LOG(INFO) << "Player " << char_id << " removed from Room ID: " << roomID;
        } else {
            throw std::runtime_error("Error removeCharacterFromRoom() called when char_id not in room.\n");
        }
    }

    const Room &RoomController::getRoom(RoomId roomID) const {
        if (auto itr = roomMap.find(roomID); itr != roomMap.end()) {
            return itr->second;
        } else {
            throw std::runtime_error("Error trying to access non existant room!");
        }
    }

    std::string RoomController::getRoomName(RoomId roomId) const {
        return getRoom(roomId).getName();
    }

    std::string RoomController::getRoomDescription(RoomId roomId) const {
        return getRoom(roomId).getDesc();
    }

    void RoomController::addOutgoingData(CharId player_id, std::string data) const {
        outgoing->operator[](player_id).emplace_back(data);
    }

    RoomController::RoomController(std::vector<Room> rooms,
                                   std::weak_ptr<CharacterController> u,
                                   std::weak_ptr<InventoryController> invC,
                                   ResponseMap out) :
            charCtrl{std::move(u)}, inventoryCtrl{std::move(invC)}, outgoing{std::move(out)} {
        roomMap.reserve(rooms.size());
        auto sharedInv = inventoryCtrl.lock();
        std::for_each(rooms.begin(), rooms.end(), [this, &sharedInv](Room r) {
            auto inventory = sharedInv->createInventory();
            r.setInventory(inventory);
            roomMap.emplace(r.getId(), std::move(r));
        });
        checkRooms();
    }

    void RoomController::addAndRemoveCharacter(CharId char_id, RoomId origin_room, RoomId dest_room) {
        LOG(INFO) << "Moving player " << char_id << " from room " << origin_room << " to " << dest_room;
        if (getRoom(origin_room).hasPlayer(char_id)) {
            removeCharacterFromRoom(char_id, origin_room);
            addCharacterToRoom(char_id, dest_room);
        } else {
            throw std::runtime_error("Error addAndRemoveCharacter() called when char_id not in origin room.\n");
        }
    }

    Room &RoomController::getMutableRoom(RoomId roomID) {
        return roomMap.at(roomID);
    }

    void RoomController::sendRoomData(CharId player_id, RoomId room_id) const {
        outgoing->operator[](player_id).emplace_back(model::toJson(getRoom(room_id), ModelType::Room));
    }

    void RoomController::checkRooms() {
        for (auto &[_, room] : roomMap) {
            std::vector<model::Door> keep;
            keep.reserve(room.getDoors().size());

            for (const auto &door : room.getDoors()) {
                if (roomExists(door.getTo())) {
                    keep.emplace_back(door);
                } else {
                    LOG(INFO) << "Removing invalid Door to " << door.getTo() << " from Room " << room.getId();
                }
            }

            room.setDoors(keep);
        }
    }

    bool RoomController::roomExists(RoomId roomID) const {
        return roomMap.find(roomID) != roomMap.end();
    }

    InventoryId RoomController::getRoomInventoryId(RoomId roomId) const{
        return getRoom(roomId).getInventory();
    }
}
