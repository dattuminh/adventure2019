#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Inventory.h"
#include "Object.h"
#include "Item.h"
#include "ID.h"

using namespace adventure::model;

TEST(InventoryTest, testInventorySize){
    Inventory inventory;
    
    Item item1;
    Item item2;

    Id id1 = 325;
    Id id2 = 425; 

    adventure::ItemId itemId1{id1};
    adventure::ItemId itemId2{id2};

    item1.setId(itemId1);
    item2.setId(itemId2);

    EXPECT_EQ(0, inventory.getSize());

    inventory.addItem(item1);

    EXPECT_EQ(1, inventory.getSize());

    inventory.addItem(item2);

    EXPECT_EQ(2, inventory.getSize());

    inventory.removeItem(itemId1);

    EXPECT_EQ(1, inventory.getSize());

    inventory.removeItem(itemId2);
    
    EXPECT_EQ(0, inventory.getSize());
}

TEST(InventoryTest, testObjectInInventory){
    Inventory inventory;
    
    Item item1;
    Item item2;

    Id id1 = 325;
    Id id2 = 425; 
    Id id3 = 500;

    adventure::ItemId itemId1{id1};
    adventure::ItemId itemId2{id2};
    adventure::ItemId itemId3{id3};

    item1.setId(itemId1);
    item2.setId(itemId2);

    EXPECT_EQ(false, inventory.itemInInventory(itemId1));
    EXPECT_EQ(false, inventory.itemInInventory(itemId2));
    EXPECT_EQ(false, inventory.itemInInventory(itemId3));
    
    inventory.addItem(item1);
    EXPECT_EQ(true, inventory.itemInInventory(itemId1));
    EXPECT_EQ(false, inventory.itemInInventory(itemId2));
    EXPECT_EQ(false, inventory.itemInInventory(itemId3));

    inventory.addItem(item2);
    EXPECT_EQ(true, inventory.itemInInventory(itemId1));
    EXPECT_EQ(true, inventory.itemInInventory(itemId2));
    EXPECT_EQ(false, inventory.itemInInventory(itemId3));

    inventory.removeItem(itemId1);
    EXPECT_EQ(false, inventory.itemInInventory(itemId1));
    EXPECT_EQ(true, inventory.itemInInventory(itemId2));
    EXPECT_EQ(false, inventory.itemInInventory(itemId3));
    
    inventory.removeItem(itemId2);
    EXPECT_EQ(false, inventory.itemInInventory(itemId1));
    EXPECT_EQ(false, inventory.itemInInventory(itemId2));
    EXPECT_EQ(false, inventory.itemInInventory(itemId3));
}
