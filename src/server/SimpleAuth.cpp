#include "Parser.h"
#include "Authentication.h"

using json = nlohmann::json;

namespace adventure::server {

    std::pair<bool, std::string> SimpleAuth::registerUserImpl(const json &data) {
        std::string err = "Error registering user. Exception: \n";
        try {
            auto itr = data.get<model::Authenticate>();
            registered_users.push_back(itr.username);
            return {true, itr.username};
        } catch (const json::exception &e) {
            LOG(ERROR) << "Exception parsing login:\n" << e.what();
            err += e.what();
        }
        return {false, err};
    }

    std::pair<bool, std::string> SimpleAuth::loginUserImpl(const json &data, Connection conn) {
        std::string err = "Error logging in user. Exception: \n";
        try {
            if (auto user = data.get<model::Authenticate>(); userExistsImpl(user.username)) {
                authenticated_connections.emplace_back(conn, user.username);
                return {true, user.username};
            } else {
                return {false, "Invalid user: " + user.username};
            }
        } catch (const json::exception &e) {
            LOG(ERROR) << "Exception parsing login:\n" << e.what();
            err += e.what();
        }
        return {false, err};
    }

    bool SimpleAuth::isConnectionAuthenticatedImpl(Connection c) const {
        for (const auto &[connection, _] : authenticated_connections) {
            if (connection == c) {
                return true;
            }
        }
        return false;
    }

    /*
     * TODO: Maybe have a 'null' connection to return? For example in the case where the game has responses queued for a
     * player but then the player disconnects before the loop is completed, the null connection can act as blackhole.
     */
    Connection SimpleAuth::toConnectionImpl(const std::string &name) const {
        for (const auto &[connection, n] : authenticated_connections) {
            if (name == n) {
                return connection;
            }
        }
        LOG(ERROR) << "Error connection for user: " << name << " was not found!";
        return Connection(); // TODO: Change this
    }

    std::string SimpleAuth::toUserImpl(Connection connection) const {
        for (const auto &[c, name] : authenticated_connections) {
            if (connection == c) {
                return name;
            }
        }
        LOG(ERROR) << "Error user for connection: " << connection.id << " was not found!";
        return ""; // TODO: Change this or throw error?
    }
}