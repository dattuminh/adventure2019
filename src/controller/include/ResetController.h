#ifndef ADVENTURE2019_RESETCONTROLLER_H
#define ADVENTURE2019_RESETCONTROLLER_H

#include "RoomController.h"
#include "InventoryController.h"
#include "Reset.h"

#include <chrono>
#include <memory>
#include <utility>

namespace adventure::controller {
    using adventure::model::Reset;
    using adventure::ObjectType;
    using adventure::ItemId;

    class ResetController {
    public:
        ResetController(std::vector<std::vector<Reset>> r,
                        std::weak_ptr<RoomController> roomC,
                        std::weak_ptr<CharacterController> charC,
                        std::weak_ptr<InventoryController> invC);

        void onTick(std::chrono::seconds delta);

        void checkResets();

    private:
        std::chrono::seconds reset_period = std::chrono::minutes(5); // TODO: Make this configurable

        std::vector<std::vector<Reset>> reset_groups{};

        std::weak_ptr<RoomController> room_controller;

        std::weak_ptr<CharacterController> char_controller;

        std::weak_ptr<InventoryController> inventory_controller;

        std::chrono::seconds time_elapsed = std::chrono::minutes(6);//std::chrono::seconds::zero();

        void performReset(const std::vector<Reset> &resets) const;

        std::vector<CharId> npcHandler(const Reset &reset) const;

        void doorHandler(const Reset &reset) const;

        ItemId objectHandler(const Reset &reset) const;

        void giveHandler(const Reset &reset, const std::vector<CharId> &characters) const;

        void equipHandler(const Reset &reset, const std::vector<CharId> &characters) const;

        void putHandler(const Reset &reset, ItemId object) const;
    };
}

#endif //ADVENTURE2019_RESETCONTROLLER_H
