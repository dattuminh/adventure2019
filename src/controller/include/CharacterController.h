#ifndef ADVENTURE2019_CHARACTERCONTROLLER_H
#define ADVENTURE2019_CHARACTERCONTROLLER_H

#include "Controller.h"
#include "ID.h"
#include "Character.h"
#include "NPC.h"
#include "InventoryController.h"

#include <string>
#include <unordered_map>
#include <vector>
#include <memory>

namespace adventure::controller {

    using adventure::model::Character;
    using adventure::model::NPC;
    using adventure::model::IdHasher;

    class CharacterController {

    public:
        CharacterController(std::vector<NPC> npc,
                            std::weak_ptr<InventoryController> invC);

        ~CharacterController() = default;

        // Mutators

        CharId addCharacter(std::string name);

        void setCharacterLocation(CharId userID, RoomId roomID);

        // Accessors

        RoomId getCharacterLocation(CharId id) const;

        const Character &getCharacter(CharId) const;

        /**
         * NOTE: The "Name" of the character has no special meaning, and is used as a default keyword.
         * @param id
         * @return the name of the character
         */
        std::string getCharacterName(CharId id) const;

        /**
         * Get all keywords including the name for a given vector of Characters
         * @param ids
         * @return
         */
        std::vector<std::pair<CharId, std::string>> getKeywords(const std::vector<CharId> &ids) const;

        InventoryId getCharacterInventoryId(CharId id) const;

        const NPC &getNPC(NPCId id) const { return npcs.at(id); }

        CharId spawnNPC(NPCId id, RoomId roomId);


    private:
        std::vector<Character> characters{};
        std::weak_ptr<InventoryController> inventoryCtrl;
        std::unordered_map<NPCId, NPC, StrongAliasIdHasher> npcs{};

        CharId getNewId() { return CharId{_user_id++}; }

        long long _user_id{123456}; // TODO: refactor this

        Character &getMutableCharacter(CharId);
    };
}

#endif //ADVENTURE2019_CHARACTERCONTROLLER_H
