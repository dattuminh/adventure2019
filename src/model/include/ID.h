#ifndef ADVENTURE2019_MODELS_H
#define ADVENTURE2019_MODELS_H

#include <StrongAlias.h>
#include <functional>

namespace adventure::model {
    struct Id {
        long long id{0};

        Id() = default;

        Id(long long i) : id{i} {}

        bool operator==(const adventure::model::Id &other) const { return (id == other.id); }
    };

    struct IdHasher {
        std::size_t operator()(const Id &id) const {
            using std::size_t;
            using std::hash;
            return (hash<long long>()(id.id));
        }
    };

    inline std::ostream &operator<<(std::ostream &os, const adventure::model::Id &i) { return os << i.id; };

}

namespace adventure {
    using adventure::model::Id;

    using CharId = StrongAlias<Id, struct CharIdTag>;
    using ItemId = StrongAlias<Id, struct ItemIdTag>;
    using InventoryId = StrongAlias<Id, struct InventoryIdTag>;
    using NPCId = StrongAlias<Id, struct NPCIdTag>;
    using RoomId = StrongAlias<Id, struct RoomIdTag>;
    using ObjectType = StrongAlias<Id, struct ObjectTypeTag>;

    using MP = StrongAlias<int, struct MPTag>;
    using HP = StrongAlias<int, struct HPTag>;

    struct StrongAliasIdHasher {
        template<typename T>
        std::size_t operator()(const StrongAlias<Id,T> &id) const {
            return model::IdHasher()(id.value);
        }
    };
}

#endif //ADVENTURE2019_MODELS_H
