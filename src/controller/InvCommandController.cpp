#include "InvCommandController.h"

namespace adventure::controller{
    InvCommandController::InvCommandController(std::weak_ptr<RoomController> room,
                                               std::weak_ptr<CharacterController> character,
                                               std::weak_ptr<InventoryController> inventory,
                                               ResponseMap out) :
        roomCtrl{std::move(room)},
        charCtrl{std::move(character)},
        invCtrl{std::move(inventory)},
        outgoing{std::move(out)} {}

    void InvCommandController::handleInventoryCommand(const CCommand<InventoryCommand> &command){
        auto inventoryCommand = command.data;
        switch (inventoryCommand.type){
            case InventoryAction::take: // fallthrough, get and take commands functions the same
            case InventoryAction::get: takeCommand(command); break;
            case InventoryAction::put: putCommand(command); break;
            case InventoryAction::drop: dropCommand(command); break;
            case InventoryAction::wear: wearCommand(command); break;
            case InventoryAction::remove: removeCommand(command); break;
            case InventoryAction::give: giveCommand(command); break;
        }
    }

    void InvCommandController::putCommand(const CCommand<InventoryCommand> &command) {
        auto inventoryCommand = command.data;
        auto charInventoryId = charCtrl.lock()->getCharacterInventoryId(command.id);

        auto possibleTypes = invCtrl.lock()->getAllObjectTypesByKeyword(inventoryCommand.container, charInventoryId);

        if(possibleTypes.empty()){
            sendData(command.id, generateNotOwnedError(inventoryCommand.container));
            return;
        }else if(possibleTypes.size() > 1){
            sendData(command.id, generateConfirmationResponse(possibleTypes));
            return;
        }
        possibleTypes = invCtrl.lock()->getAllObjectTypesByKeyword(inventoryCommand.item, charInventoryId);
        if(possibleTypes.empty()){
            sendData(command.id, generateNotOwnedError(inventoryCommand.item));
            return;
        }else if(possibleTypes.size() > 1){
            sendData(command.id, generateConfirmationResponse(possibleTypes));
            return;
        }

        auto containerId = invCtrl.lock()->getItemIdByKeyword(inventoryCommand.container, charInventoryId);
        auto itemId = invCtrl.lock()->getItemIdByKeyword(inventoryCommand.item, charInventoryId);
        invCtrl.lock()->putItemInContainer(containerId, itemId, charInventoryId);

        // return success message
        std::ostringstream oss;
        oss << "You've successfully put " << inventoryCommand.item
            << " inside " << inventoryCommand.container << "\n";

        sendData(command.id, ChatMessage{"Server", oss.str()});
    }

    void InvCommandController::takeCommand(const CCommand<InventoryCommand> &command) {
        auto roomId = charCtrl.lock()->getCharacterLocation(command.id);
        auto roomInventoryId = roomCtrl.lock()->getRoomInventoryId(roomId);
        auto inventoryCommand = command.data;
        auto charInventoryId = charCtrl.lock()->getCharacterInventoryId(command.id);

        auto possibleTypes = invCtrl.lock()->getAllObjectTypesByKeyword(inventoryCommand.item, roomInventoryId);
        if(possibleTypes.empty()) {
            sendData(command.id, generateNotInRoomError(inventoryCommand.item));
            return;
        }
        if(possibleTypes.size() > 1){
            sendData(command.id, generateConfirmationResponse(possibleTypes));
            return;
        }

        auto itemId = invCtrl.lock()->getItemIdByKeyword(inventoryCommand.item, roomInventoryId);
        invCtrl.lock()->moveItem(itemId, roomInventoryId, charInventoryId);
        auto obj = invCtrl.lock()->getObjectByItemId(itemId, charInventoryId);
        sendData(command.id, model::toJson(obj, ModelType::Object));
    }

    void InvCommandController::dropCommand(const CCommand<InventoryCommand> &command) {
        auto roomId = charCtrl.lock()->getCharacterLocation(command.id);
        auto roomInventoryId = roomCtrl.lock()->getRoomInventoryId(roomId);
        auto inventoryCommand = command.data;
        auto charInventoryId = charCtrl.lock()->getCharacterInventoryId(command.id);

        auto possibleTypes = invCtrl.lock()->getAllObjectTypesByKeyword(inventoryCommand.item, charInventoryId);
        if(possibleTypes.empty()){
            sendData(command.id, generateNotOwnedError(inventoryCommand.item));
            return;
        }
        if(possibleTypes.size() > 1){
            sendData(command.id, generateConfirmationResponse(possibleTypes));
            return;
        }

        auto itemId = invCtrl.lock()->getItemIdByKeyword(inventoryCommand.item, charInventoryId);
        invCtrl.lock()->moveItem(itemId, charInventoryId, roomInventoryId);
        ChatMessage msg{"Server", "Item is dropped."};
        sendData(command.id, msg);
    }

    void InvCommandController::wearCommand(const CCommand<InventoryCommand> &command) {
        auto inventoryCommand = command.data;
        auto charInventoryId = charCtrl.lock()->getCharacterInventoryId(command.id);

        auto possibleTypes = invCtrl.lock()->getAllObjectTypesByKeyword(inventoryCommand.item, charInventoryId);
        if(possibleTypes.empty()){
            sendData(command.id, generateNotOwnedError(inventoryCommand.item));
            return;
        }
        if(possibleTypes.size() > 1){
            sendData(command.id, generateConfirmationResponse(possibleTypes));
            return;
        }

        auto itemId = invCtrl.lock()->getItemIdByKeyword(inventoryCommand.item, charInventoryId);
        auto item = invCtrl.lock()->removeItem(itemId, charInventoryId);
        // if slot already has item -> swap back to inventory
        if(!invCtrl.lock()->isSlotEmpty(inventoryCommand.slot, command.id)){
            auto itemInSlot = invCtrl.lock()->unequipItem(inventoryCommand.slot, command.id);
            invCtrl.lock()->addItem(itemInSlot, charInventoryId);
        }
        invCtrl.lock()->equipItem(item, inventoryCommand.slot, command.id);

        auto obj = invCtrl.lock()->getObject(item.getType());
        sendData(command.id, model::toJson(obj, ModelType::Object));
    }

    void InvCommandController::removeCommand(const CCommand<InventoryCommand> &command) {
        auto inventoryCommand = command.data;
        auto charInventoryId = charCtrl.lock()->getCharacterInventoryId(command.id);

        auto possibleTypes = invCtrl.lock()->getAllObjectTypesByKeyword(inventoryCommand.item, charInventoryId);
        if(possibleTypes.empty()){
            sendData(command.id, generateNotOwnedError(inventoryCommand.item));
            return;
        }

        if(possibleTypes.size() > 1){
            sendData(command.id, generateConfirmationResponse(possibleTypes));
            return;
        }

        if(invCtrl.lock()->isSlotEmpty(inventoryCommand.slot, command.id)){
            sendData(command.id, model::ErrorMessage{.message="Slot is empty", .type=ModelType::Inventory});
            return;
        }

        auto item = invCtrl.lock()->unequipItem(inventoryCommand.slot, command.id);
        invCtrl.lock()->addItem(item, charInventoryId);
        ChatMessage msg{"Server", "Unequiped item."};
        sendData(command.id, msg);
    }

    void InvCommandController::giveCommand(const CCommand<InventoryCommand> &command) {
        auto inventoryCommand = command.data;
        auto roomId = charCtrl.lock()->getCharacterLocation(command.id);
        auto room = roomCtrl.lock()->getRoom(roomId);
        auto keywords = charCtrl.lock()->getKeywords(room.getCharacters());
        auto receiver = inventoryCommand.giveTo;
        auto giverInventory = charCtrl.lock()->getCharacterInventoryId(command.id);
        auto possibleTypes = invCtrl.lock()->getAllObjectTypesByKeyword(inventoryCommand.item, giverInventory);

        if(possibleTypes.empty()){
            sendData(command.id, generateNotOwnedError(inventoryCommand.item));
            return;
        }else if(possibleTypes.size() > 1){
            sendData(command.id, generateConfirmationResponse(possibleTypes));
            return;
        }

        std::vector<CharId> charIds;

        std::for_each(keywords.begin(), keywords.end(), [&](auto pair){
            if(pair.second == receiver){
                charIds.emplace_back(pair.first);
            }
        });
        if(charIds.empty()){
            sendData(command.id, generateCannotFindChar(receiver));
            return;
        }else if(charIds.size() > 1){
            std::vector<std::string> characters;
            auto charC = charCtrl.lock();
            for(auto id : charIds){
                auto character = charC->getCharacterName(id);
                characters.emplace_back(character);
            }
            sendData(command.id, model::Confirmation<std::string>{.options=characters, .type=ModelType::Character});
            return;
        }

        auto receiverId = charIds.front();
        auto receiverInventory = charCtrl.lock()->getCharacterInventoryId(receiverId);

        auto itemId = invCtrl.lock()->getItemIdByKeyword(inventoryCommand.item, giverInventory);
        invCtrl.lock()->moveItem(itemId, giverInventory, receiverInventory);

        // return success messages
        std::ostringstream oss;
        oss << "You gave " << inventoryCommand.item
            << " to " << inventoryCommand.giveTo << "\n";
        sendData(command.id, ChatMessage{"Server", oss.str()});

        oss.clear();
        oss << charCtrl.lock()->getCharacterName(command.id) << " gave you a " << inventoryCommand.item << "\n";
        sendData(command.id, ChatMessage{"Server", oss.str()});
    }

    void InvCommandController::sendData(CharId charId, const json &message) {
        outgoing->operator[](charId).emplace_back(message);
    }
    model::ErrorMessage InvCommandController::generateNotOwnedError(std::string_view item){
        std::ostringstream oss;
        oss << "Item "<< item
            << " is not in your inventory\n";
        return model::ErrorMessage{.message=oss.str(), .type=ModelType::Object};
    }

    model::ErrorMessage InvCommandController::generateNotInRoomError(std::string_view item){
        std::ostringstream oss;
        oss << "Item "<< item
            << " is not in the room\n";
        return model::ErrorMessage{.message=oss.str(), .type=ModelType::Object};
    }

    model::ErrorMessage InvCommandController::generateCannotFindChar(std::string_view charName) {
        std::ostringstream oss;
        oss << "Cannot find "<< charName
            << " in the room\n";
        return model::ErrorMessage{.message=oss.str(), .type=ModelType::Character};
    }

    model::Confirmation<Object> InvCommandController::generateConfirmationResponse(const std::vector<ObjectType> &types) {
        std::vector<Object> objs;

        auto invC = invCtrl.lock();
        for(auto type : types){
            objs.emplace_back(invC->getObject(type));
        }
        return model::Confirmation<Object>{.options=objs, .type=ModelType::Object};
    }


}
