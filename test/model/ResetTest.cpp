#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Reset.h"
#include "ID.h"
#include <string>
#include <utility>
#include <string>
#include <enum.h>
#include <ostream>

using namespace adventure::model;
using adventure::RoomId;

TEST(ResetTest, testingReset) {
    typedef adventure::model::ResetAction Action;
    Action action{Action::Equip};
    Id resetId = 8080;
    int limit = 1000;
    RoomId roomId{8081};
    int slot = 17;
    Reset reset;

    reset.setAction(action);
    reset.setId(resetId);
    reset.setLimit(limit);
    reset.setRoom(roomId);
    reset.setSlot(slot);

    EXPECT_EQ(action._to_string(), reset.getAction()._to_string());
    EXPECT_EQ(resetId, reset.getId());
    EXPECT_EQ(limit, reset.getLimit());
    EXPECT_EQ(roomId, reset.getRoom());
    EXPECT_EQ(slot, reset.getSlot());
}

