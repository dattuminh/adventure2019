#include "PlayerController.h"

using namespace adventure::controller;

CharId PlayerController::addPlayer(const std::string &username) {
    auto id = charCtrl.lock()->addCharacter(username);
    players.emplace(username, Player{username, id});
    avatars.emplace(id, username);
    return id;
}

bool PlayerController::characterIsAvatar(CharId id) const {
    return avatars.find(id) != avatars.end();
}

CharId PlayerController::getActiveAvatar(const std::string &name) const {
    return players.at(name).getActiveAvatar();
}

std::string PlayerController::avatarToPlayerName(CharId id) const {
    return avatars.at(id);
}
