add_library(game_client GameClient.cpp ValidateInput.cpp)

target_include_directories(game_client
        PUBLIC
        $<INSTALL_INTERFACE:include>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        )

target_link_libraries(game_client
        model
        view
        controller
        networking
        libadventure)
