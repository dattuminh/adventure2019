#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Room.h"
#include "Extra.h"
#include "Door.h"
#include "ID.h"
#include <string>
#include <cassert>

using namespace adventure::model;
using adventure::RoomId;

// Tests id, name, desc of a room
TEST(RoomTest, testingRoomContent) {
    RoomId id{20};
    std::string name = "room1";
    std::string desc = "desc1";
    Room room;

    room.setId(id);
    room.setName(name);
    room.setDesc(desc);

    EXPECT_EQ(id, room.getId());
    EXPECT_EQ(name, room.getName());
    EXPECT_EQ(desc, room.getDesc());
}

// Tests add and remove a character in a room
TEST(RoomTest, testingCharacter) {
    Id id = 2020;
    adventure::CharId characterId{id};
    Room room;

    EXPECT_FALSE(room.hasPlayer(characterId));

    room.addCharacter(characterId);
    EXPECT_TRUE(room.hasPlayer(characterId));

    room.removeCharacter(characterId);
    EXPECT_FALSE(room.hasPlayer(characterId));
}

// Tests Extra
TEST(RoomTest, testingExtra) {
    std::vector<std::string> keywords = {"box", "jacket"};
    std::vector<std::string> longdesc = {"google", "testing"};

    Room room;
    Extra extra;
    extra.setKeywords(keywords);
    extra.setDescription("string");
    std::vector<Extra> extras = {extra};

    room.setExtendedDescriptions(extras);
    EXPECT_TRUE(room.getExtendedDescriptions().size() == 1);
    EXPECT_EQ(extras[0].getKeywords(), room.getExtendedDescriptions()[0].getKeywords());
    EXPECT_EQ(extras[0].getDescription(), room.getExtendedDescriptions()[0].getDescription());
}

// Tests Door
TEST(RoomTest, testingDoor){
    adventure::Direction direction{adventure::Direction::north};
    typedef adventure::model::DoorState State;
    State state{adventure::model::DoorState::Open};
    std::string desc = "test";
    std::vector<std::string> keywords = {"box", "jacket"};
    RoomId to{2020};

    Room room;
    Door door;
    door.setDirection(direction);
    door.setState(state);
    door.setDescription(desc);
    door.setKeywords(keywords);
    door.setTo(to);
    std::vector<Door> doors = {door};

    room.setDoors(doors);

    EXPECT_EQ(door, room.getDoor(direction));        // causes errors
    EXPECT_TRUE(room.getDoors().size() == 1);
    EXPECT_TRUE(room.hasDirection(direction));
    EXPECT_EQ(doors[0].getTo(), room.getDoors()[0].getTo());
    EXPECT_EQ(doors[0].getState()->_to_string(), room.getDoors()[0].getState()->_to_string());
}