#ifndef ADVENTURE2019_STRONGALIAS_H
#define ADVENTURE2019_STRONGALIAS_H

#include <utility>
#include <ostream>

/**
 * This provides a strongly typed wrapper to prevent argument confusion.
 * @author Dr. Nick Sumner
 * @tparam Base The underlying datatype
 * @tparam Tag
 */
template<typename Base, typename Tag>
struct StrongAlias {
    explicit StrongAlias(Base value) : value{std::move(value)} {}

    StrongAlias() { value = {}; };
    Base value;

    bool operator==(const StrongAlias<Base, Tag> &a) const {
        return this->value == a.value;
    }

    bool operator()(const StrongAlias<Base, Tag> &a) const {
        return this->value == a.value;
    }
};

template<typename Base, typename Tag>
inline std::ostream &operator<<(std::ostream &os, const StrongAlias<Base, Tag> &i) { return os << i.value; };

#endif //ADVENTURE2019_STRONGALIAS_H
