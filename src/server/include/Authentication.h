#ifndef ADVENTURE2019_AUTH_H
#define ADVENTURE2019_AUTH_H

#include <nlohmann/json.hpp>
#include <algorithm>
#include <string>
#include <utility>
#include <vector>
#include <Server.h>

using json = nlohmann::json;
using networking::Connection;

namespace adventure::server {
    /*
     * Abstract Class so there can be different methods of auth, such as no auth, passwords, LDAP, some database, etc.
     * pair<bool, string>, where the bool is if the operation was succesful or not, and the string is the user name
     * when succesful, or an error message when unsuccesful.
     * Any method that might have to handle more than a single param accepts a json type that is implementation defined
     */
    class AuthenticationHandler {
    public:
        virtual ~AuthenticationHandler() = default;

        bool userExists(const std::string &s) const { return userExistsImpl(s); }

        std::pair<bool, std::string> registerUser(const json &data) { return registerUserImpl(data); }

        std::pair<bool, std::string> loginUser(const json &data, Connection conn) { return loginUserImpl(data, conn); }

        const std::vector<std::string> &getAllUserNames() const { return getAllUserNamesImpl(); }

        bool isConnectionAuthenticated(Connection c) const { return isConnectionAuthenticatedImpl(c); }

        Connection toConnection(const std::string &s) const { return toConnectionImpl(s); }

        std::string toUser(Connection c) const { return toUserImpl(c); }

        unsigned long authenticatedConnectionCount() const { return authenticatedConnectionCountImpl(); }


    private:
        virtual bool userExistsImpl(const std::string &) const = 0;

        virtual std::pair<bool, std::string> registerUserImpl(const json &data) = 0;

        virtual std::pair<bool, std::string> loginUserImpl(const json &data, Connection conn) = 0;

        virtual const std::vector<std::string> &getAllUserNamesImpl() const = 0;

        virtual bool isConnectionAuthenticatedImpl(Connection) const = 0;

        virtual Connection toConnectionImpl(const std::string &) const = 0;

        virtual std::string toUserImpl(Connection) const = 0;

        virtual unsigned long authenticatedConnectionCountImpl() const = 0;
    };

    /*
     * An implementation of AuthenticationHandler used for testing, debugging, and demo purposes.
     * It purposely accepts any values, and does not do any error checking.
     */
    class SimpleAuth : public AuthenticationHandler {
    public:
        SimpleAuth() = default;

    private:
        bool userExistsImpl(const std::string &s) const override {
            return std::find(registered_users.begin(), registered_users.end(), s) != registered_users.end();
        };

        std::pair<bool, std::string> registerUserImpl(const json &data) override;

        std::pair<bool, std::string> loginUserImpl(const json &data, Connection conn) override;

        const std::vector<std::string> &getAllUserNamesImpl() const override { return registered_users; };

        bool isConnectionAuthenticatedImpl(Connection) const override;

        Connection toConnectionImpl(const std::string &) const override;

        std::string toUserImpl(Connection) const override;

        unsigned long authenticatedConnectionCountImpl() const override { return authenticated_connections.size(); }

        std::vector<std::string> registered_users{"user1", "user2"};
        std::vector<std::pair<Connection, std::string>> authenticated_connections{};
    };
}

#endif //ADVENTURE2019_AUTH_H
