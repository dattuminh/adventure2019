#ifndef ADVENTURE2019_MOVEMENTCONTROLLER_H
#define ADVENTURE2019_MOVEMENTCONTROLLER_H

#include "Room.h"
#include "Parser.h"
#include "RoomController.h"
#include "CharacterController.h"

#include <vector>
#include <memory>
#include <unordered_map>

namespace adventure::controller {
    using adventure::model::Door;
    using adventure::model::GameDataWrapper;

    class MovementController {
    public:
        MovementController(std::weak_ptr<RoomController> room,
                           std::weak_ptr<CharacterController> character,
                           RoomId def_room,
                           ResponseMap out);

        void handleMoveCommand(const CCommand<model::Move> &command) const;

        /*
         * Spawns the Character in the default room.
         */
        void spawnCharacter(CharId character_id) const;

    private:
        std::weak_ptr<RoomController> roomCtrl;
        std::weak_ptr<CharacterController> charCtrl;
        const RoomId defaultRoom;
        ResponseMap outgoing;

        /**
         * Checks if a door is open. If not, sends the Character the reason why.
         * @param id The Character performing the action
         * @param door
         * @return True if Open, false if not. Nil is considered to be open.
         */
        bool doorIsOpen(CharId id, const Door &door) const;

        void addOutgoingChat(CharId, const std::string &) const;

    };
}

#endif //ADVENTURE2019_MOVEMENTCONTROLLER_H
