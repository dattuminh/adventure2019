#include "GameServer.h"

namespace adventure::server {

    GameServer::GameServer(unsigned short port, const char *data_dir, long max_connections) :
            authHandler{std::make_unique<SimpleAuth>()}, max_connections(max_connections) {
        try {
            // Placeholder for web app client
            const auto HTML_PAGE = R"(<!DOCTYPE html><html><head> <meta charset="utf-8"/> <title>Web Chat</title> <style>body, input, select, textarea{background: #031e11; color: #10fd60;}#messages{width: 80em; height: 40em; border: solid 1px #cccccc; margin-bottom: 5px; overflow-y: hidden;}</style> <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> <script>$(document).ready(function(){function appendText(text){$('#messages').append(text); $('#messages').scrollTop($('#messages')[0].scrollHeight);}var ws=null; var uriPieces=window.location.href.split('/'); var wsLocation='ws://' + uriPieces[2]; $('#uri:text').val(wsLocation); $('#connect').click(function(){ws=new WebSocket(uri.value); ws.onopen=function(ev){appendText("Connected!\n");}; ws.onclose=function(ev){appendText("Disconnected!\n");}; ws.onmessage=function(ev){appendText(ev.data + "\n");}; ws.onerror=function(ev){appendText("[error]\n"); console.log(ev);};}); $('#disconnect').click(function (){ws.close();}); $('#send').click(function (){ws.send(sendMessage.value); sendMessage.value="";}); $('#sendMessage').keyup(function(e){e.preventDefault(); if (e.keyCode===13){$('#send').click();}});}); </script></head><body> <h1>Web Chat</h1> Chat Server: <input id="uri" size="40"> <button id="connect">Connect</button> <button id="disconnect">Disconnect</button><br/> <pre id="messages"></pre> <div style="margin-bottom: 5px;"> Enter Message:<br/> <input id="sendMessage" size="80" value=""/> <button id="send">Send</button> </div></body></html>)";

            this->server = std::make_unique<Server>(port,
                                                    HTML_PAGE,
                    /* Lambdas to private non-static class method */
                                                    [this](auto c) { this->onConnect(c); },
                                                    [this](auto d) { this->onDisconnect(d); }
                                                   );
        } catch (const boost::system::system_error &e) { // Treat inability to start server as a fatal error
            LOG(FATAL) << "Exception initializing server\n" << e.what() << "\n"
                      << "Game Server Could not start. Now exiting the program.";
            // TODO: Check if there is a way to see why server failed to start, ex: port in use vs some other kind of error.
            exit(-1);
        }

        this->game = std::make_unique<controller::GameController>(model::loadAreasFromFiles(data_dir));
        LOG(INFO) << "Server Initialized! Found " << std::to_string(this->game->serverStatus().areas.size())
                  << " areas.";
    }

    /*
     * Main Server Loop. This is the core loop for the server, it performs a server.update(), then asks the Game to do a
     * tick, then it passes the game the incoming requests. Then the responses are prepared and finally sent to clients.
     * Rinse and repeat.
     */
    int GameServer::runServer() {
        while (game->gameIsRunning()) { // Main server loop
            tryServerUpdate();

            game->performGameTick();

            incoming = server->receive();

            processMessages(incoming);

            prepareOutgoingFromGame();

            server->send(outgoing);

            sleep(1);

            outgoing.clear();
        } // server loop

        LOG(INFO) << "Gracefully Shutting server down";
        disconnectClients();
        return 0;
    }

    /*
     * Checks each message for syntax validity (It is recognized as a json and has an Action), then checks if the
     * connection is authenticated or not.
     */
    void GameServer::processMessages(std::deque<Message> &incoming_messages) {
        while (!incoming_messages.empty()) {
            const auto msg = incoming_messages.front();
            incoming_messages.pop_front();

            json j = parseMessageToJson(msg);
            if (syntaxCheckJsonAction(j)) {
                LOG(INFO) << "Incoming: " << j.dump();
                if (authHandler->isConnectionAuthenticated(msg.connection)) {
                    processAction(msg, j);
                } else {
                    // Assume that if user is not authenticated then it is an auth request
                    unauthenticatedMessageHandler(msg, j);
                }
            } else {
                LOG(INFO) << "No action found in request: " << msg.text;
                sendChatMessage(msg.connection, "Not a valid action: " + msg.text);
            }
        }
    }

    void GameServer::disconnectClients() {
        for (const auto c : clients) {
            // TODO: Stub
            sendChatMessage(c, "Goodbye!");
        }
        // Send out remaining responses before disconnecting
        server->send(outgoing);
        outgoing.clear();
        for (auto c : clients) {
            server->disconnect(c);
        }
    }

    void GameServer::onConnect(Connection c) {
        LOG(INFO) << "New connection found: " << c.id;
        clients.push_back(c);
    }

    void GameServer::onDisconnect(Connection c) {
        LOG(INFO) << "Connection lost: " << c.id;
        // TODO: Need to lgout with AuthHandler and notify game
        auto eraseBegin = std::remove(std::begin(clients), std::end(clients), c);
        clients.erase(eraseBegin, std::end(clients));
    }

    void GameServer::debugAction(const json &j, Connection connection) {
        auto itr = j.find("DebugCmd");
        Message m = {.connection = connection};
        std::ostringstream result;
        if (itr != j.end()) {
            auto option = j.at("DebugCmd").get<std::string>();
            // TODO: refactor into somewhere else?
//            if (option == "dump") {
//                for (const auto &r : game->getResets("Mirkwood")) {
//                    result << "Action: " << r.getAction() << " ID: " << r.getId() << " Limit: " << r.getLimit() <<
//                           " Room: " << r.getRoom() << " Slot: " << r.getSlot() << " State: " << r.getState()
//                           << std::endl;
//                }
//                m.text = result.str();
//            }
        } else {
            m.text = "Invalid option";
        }
        outgoing.emplace_back(std::move(m));
    }

    void GameServer::unauthenticatedMessageHandler(const Message &c, const json &j) {
        AuthResponse response;
        std::pair<bool, std::string> result;

        switch (getAction(j)) {
            case Action::Authenticate: {
                result = authHandler->loginUser(j, c.connection);
                response.action = Action::Authenticate;
                break;
            }
            case Action::Register: {
                result = authHandler->registerUser(j);
                response.action = Action::Register;
                break;
            }
            default: {
                LOG(ERROR) << "Unauthenticated message wasn't Authenticate or Register:\n" << c.text;
                break;
            }

        }
        response.authenticated = result.first;
        response.text = result.second;

        json a = response;
        outgoing.emplace_back(Message{.connection = c.connection, .text = a.dump(DEBUG_JSON_INDENT)});
    }

    json GameServer::parseMessageToJson(const Message &msg) {
        try {
            return json::parse(msg.text);
            // TODO: maybe do some checking here for json schema?
        } catch (const json::exception &e) {
            LOG(WARNING) << "Ignoring invalid message as JSON: " << e.what();
            sendChatMessage(msg.connection, "Invalid data received: " + msg.text);
        }
        return nullptr;
    }

    void GameServer::processAction(const Message &msg, const json &j) {

        Action incoming_action = getAction(j);

        switch (incoming_action) {
            case Action::Nil: {
                // TODO: Handle invalid action
            }
                // Server frontend actions
            case Action::Heartbeat: {
                processHeartbeat(msg.connection);
                break;
            }
            case Action::Debug: {
                debugAction(j, msg.connection);
                break;
            }
            case Action::Status: {
                statusRequest(msg.connection);
                break;
            }
            case Action::Shutdown: {
                game->shutdownGame();
                break;
            }
            default: // All other Actions are wrapped as a Command and sent to Game
            case Action::ChatCommand: {
                game->receiveGameData(
                        GameDataWrapper{.connection_identifier = authHandler->toUser(msg.connection), .data = j});
                break;
            }
        }
    }

    /*
     * Check if a JSON object is not null, and is syntactically conformant to a game Action.
     */
    bool GameServer::syntaxCheckJsonAction(const json &json) {
        return !json.is_null() && json.find("Action") != json.end();
    }

    /**
     * Get outgoing responses from the Game, map the destination to a Connection, then add to outgoing.
     */
    void GameServer::prepareOutgoingFromGame() {
        auto game_messages = game->getOutgoing();
        while (!game_messages.empty()) {
            auto r = game_messages.front();
            game_messages.pop_front();

            outgoing.emplace_back(
                    Message{.connection = authHandler->toConnection(r.connection_identifier),
                            // Indent the json so it's easier to read
                            .text = r.data.dump(DEBUG_JSON_INDENT)});
        }
    }

    void GameServer::processHeartbeat(Connection c) {
        // TODO: Should heartbeat messages be something else?
        outgoing.emplace_back(Message{.connection = c, .text = "Heartbeat OK"});
    }

    void GameServer::statusRequest(Connection connection) {
        ServerStatus s = game->serverStatus();
        s.max_players = max_connections;
        s.player_count = authHandler->authenticatedConnectionCount();
        json j = s;
        outgoing.emplace_back(Message{.connection = connection, .text = j.dump(DEBUG_JSON_INDENT)});
    }

    void GameServer::tryServerUpdate() {
        try {
            server->update();
        } catch (const std::exception &e) {
            LOG(FATAL) << "Exception from Server update:\n" << e.what();
            // TODO: Maybe not shutdown for this.
            // TODO: See if a server update exception is fatal or not
            game->shutdownGame();
        }
    }

    void GameServer::sendChatMessage(Connection c, const std::string &msg) {
        ChatMessage chat{.from = "GameServer", .text = msg};
        json j = chat;
        outgoing.emplace_back(Message{.connection = c, .text = j.dump(DEBUG_JSON_INDENT)});
    }
}
