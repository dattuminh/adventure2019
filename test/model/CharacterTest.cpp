#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Character.h"
#include "ID.h"
#include <string>

using namespace adventure::model;
using adventure::RoomId;

// Tests character id and name 
TEST(CharacterTest, testingCharacteId) {
    Id id = 2020;
    std::string name = "Toronto Raptors";
    adventure::CharId charId{id};
    Character character;


    character.setId(charId);
    EXPECT_EQ(charId, character.getId());

    character.setName(name);
    EXPECT_EQ(name, character.getName());
}

// Tests location
TEST(CharacterTest, testingLocation) {
    RoomId loc{0101};
    Character character;

    character.setLocation(loc);
    EXPECT_EQ(loc, character.getLocation());
}

// Tests health
TEST(CharacterTest, testingHP) {
    adventure::HP health;
    Character character;

    EXPECT_EQ(100, character.getHealth().value);

    character.changeHealth(-10);    // test decrease
    EXPECT_EQ(90, character.getHealth().value);

    character.changeHealth(50);     // test over increase
    EXPECT_EQ(90, character.getHealth().value);

    character.changeHealth(-110);   // test over decrease
    EXPECT_EQ(90, character.getHealth().value);
}

// Tests shortdesc, longdesc, description, and keywords
TEST(CharacterTest, testingDescription){
    std::string shortdesc = "this is a test";
    std::vector<std::string> longdesc = {"unbelievable history", "is this a joke?"};
    std::string description = "just a description";
    std::vector<std::string> keywords = {"wow", "chow"};
    Character character;    

    character.setShortDesc(shortdesc);
    character.setLongDesc(longdesc);
    character.setDescription(description);
    character.setKeywords(keywords);
    
    EXPECT_EQ(shortdesc, character.getShortDesc());
    EXPECT_EQ(longdesc, character.getLongDesc());
    EXPECT_EQ(description, character.getDescription());
    EXPECT_EQ(keywords, character.getKeywords());
}

