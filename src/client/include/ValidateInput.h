#ifndef ADVENTURE2019_VALIDATEINPUT_H
#define ADVENTURE2019_VALIDATEINPUT_H

#include <nlohmann/json.hpp>
#include "Parser.h"
#include "Adventure.h"
#include "StructModels.h"

using adventure::model::to_json;
using adventure::model::Command;
using adventure::model::Authenticate;
using adventure::Action;
using adventure::CommCommands;
using adventure::InventoryAction;
using adventure::model::JoinArea;
using adventure::model::Debug;
using adventure::model::Move;
using adventure::model::ChatCommand;
using adventure::model::LookCommand;
using adventure::model::InventoryCommand;
using json = nlohmann::json;

namespace adventure::client {
    class ValidateInput{
    public:
        ValidateInput();
        ~ValidateInput() = default;

        bool isValidCommandType(const std::string_view& userInput);

        std::string chatInputToJsonString(const std::string_view &input);
        std::string authInputToJsonString(const std::string &username, const std::string &password);
    private:
        std::map<std::string_view, Action> commandActionPairs;
        json createJson(const std::string &command, const std::string &args, const Action &action);
        Action getActionFromString(const std::string_view &commandStr);

    };

}

#endif //ADVENTURE2019_VALIDATEINPUT_H
