#ifndef ADVENTURE2019_ROOM_H
#define ADVENTURE2019_ROOM_H

#include "Player.h"
#include "Door.h"
#include "Extra.h"
#include "StructModels.h"
#include "Inventory.h"
#include "Object.h"
#include "ID.h"

#include <glog/logging.h>
#include <string>
#include <vector>
#include <algorithm>

namespace adventure::model {

    class Room {
    public:
        Room() = default;
        virtual ~Room() = default;

    private:
        RoomId id{};
        std::string name;
        std::string desc;
        std::vector<Door> doors{};
        std::vector<Extra> extended_descriptions{};
        std::vector<CharId> character_ids{};
        InventoryId inventory;

    public:
        RoomId getId() const { return id; }

        void setId(RoomId value) { this->id = value; }

        std::string getName() const { return name; }

        void setName(const std::string &value) { this->name = value; }

        std::string getDesc() const { return desc; }

        void setDesc(const std::string &desc) { this->desc = desc; }

        const std::vector<Door> &getDoors() const { return doors; }

        void setDoors(const std::vector<Door> &value) { this->doors = value; }

        const std::vector<Extra> &getExtendedDescriptions() const { return extended_descriptions; }

        void setExtendedDescriptions(const std::vector<Extra> &value) { this->extended_descriptions = value; }

        bool hasDoorIndex(Id id) const { return doors.size() > static_cast<unsigned long>(id.id); }

        const Door &getDoorByIndex(Id id) const { return doors.at(static_cast<unsigned long>(id.id)); };

        Door &getMutableDoorByIndex(Id id) { return doors.at(static_cast<unsigned long>(id.id)); };

        const std::vector<CharId> &getCharacters() const { return character_ids; }

        // keep track of all players
        void addCharacter(CharId id) { character_ids.emplace_back(id); }

        void removeCharacter(CharId id) {
            auto it = std::find_if(character_ids.begin(), character_ids.end(), id);

            if (it != character_ids.end()) {
                character_ids.erase(it);
            }
            else{
                LOG(ERROR) << "tried to remove a Character from room " << name << " but charID " << id << " was not here!";
            }
        }

        // TODO: double check
        bool hasDirection(Direction direction) const {
            auto it = std::find_if(doors.begin(), doors.end(), [&](Door door) {
                return door.getDirection() == direction;
            });

            return it != doors.end();
        }

        // get the door that the direction leads to
        Door getDoor(Direction direction) const {
            for (const Door &door : doors) {
                if (door.getDirection() == direction) {
                    return door;
                }
            }
            LOG(FATAL) << "getDoor() called with non-existant Direction: " << direction;
            throw std::runtime_error("getDoor() called with non-existant Direction.");
        }

        bool hasPlayer(CharId id) const {
            return std::find(character_ids.begin(), character_ids.end(), id) != character_ids.end();
        }

        InventoryId getInventory() const { return inventory; }

        void setInventory(InventoryId id) { this->inventory = id; }
    };
}

#endif //ADVENTURE2019_ROOM_H
