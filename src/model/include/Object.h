#ifndef ADVENTURE2019_OBJECT_H
#define ADVENTURE2019_OBJECT_H

#include "Extra.h"
#include "ID.h"
#include "StructModels.h"

#include <string>
#include <vector>
#include <utility>

namespace adventure::model {

/**
* @class Object
*
* @brief A class to handle the inventory of objects that characters may have.
*/
    class Object {
    public:
        Object() = default;

    private:
        ObjectType id;
        std::string shortdesc;
        std::vector<std::string> keywords;
        std::vector<std::string> longdesc;
        std::vector<Extra> extra;

    public:

        ObjectType getId() const { return id; };

        void setId(ObjectType id) { this->id = id; };

        std::string getShortDesc() const { return shortdesc; };

        void setShortDesc(const std::string &shortdesc) { this->shortdesc = shortdesc; };

        std::vector<std::string> getKeywords() const { return keywords; };

        void setKeywords(const std::vector<std::string> &keywords) { this->keywords = keywords; };

        std::vector<std::string> getLongDesc() const { return longdesc; };

        void setLongDesc(const std::vector<std::string> &longdesc) { this->longdesc = longdesc; };

        std::vector<Extra> getExtra() const { return extra; };

        void setExtra(const std::vector<Extra> &extra) { this->extra = extra; };
    };

}

#endif // ADVENTURE2019_OBJECT_H
