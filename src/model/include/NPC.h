#ifndef ADVENTURE2019_NPC_H
#define ADVENTURE2019_NPC_H

#include "ID.h"

#include <string>
#include <utility>
#include <vector>


namespace adventure::model {
    class NPC {
    private:
        NPCId id{};
        std::string shortdesc;
        std::vector<std::string> keywords;
        std::vector<std::string> longdesc;
        std::string description;
        std::vector<std::pair<CharId, RoomId>> characters;

    public:
        NPC() = default;
        ~NPC() = default;

        void setId(NPCId val) { id = val; };

        NPCId getId() const { return id; };

        void setShortDesc(const std::string &val) { shortdesc = val; };

        std::string getShortDesc() const { return shortdesc; };

        void setKeywords(const std::vector<std::string> &val) { keywords = val; };

        std::vector<std::string> getKeywords() const { return keywords; };

        void setLongDesc(const std::vector<std::string> &val) { longdesc = val; };

        std::vector<std::string> getLongDesc() const { return longdesc; };

        void setDescription(const std::string &val) { description = val; };

        std::string getDescription() const { return description; };

        std::vector<CharId> getCharacters(RoomId room_id) const;

        void addCharacter(std::pair<CharId, RoomId>);

    };
}


#endif //CMPT373_NPC_H
