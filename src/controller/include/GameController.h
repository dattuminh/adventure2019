#ifndef ADVENTURE2019_GAMECONTROLLER_H
#define ADVENTURE2019_GAMECONTROLLER_H

#include "AreaController.h"
#include "StructModels.h"

#include <glog/logging.h>
#include <vector>
#include <string>
#include <map>
#include <nlohmann/json.hpp>
#include <chrono>

namespace adventure::controller {
    using adventure::model::Area;
    using adventure::model::GameDataWrapper;
    using adventure::model::ServerStatus;
    using adventure::model::JoinArea;

    class GameController {
    public:
        GameController(std::vector<Area> a);

        bool gameIsRunning() const { return is_game_running; };

        void receiveGameData(GameDataWrapper message);

        model::ServerStatus serverStatus() const;

        std::list<GameDataWrapper> getOutgoing();

        void performGameTick();

        void shutdownGame();

        void joinGame(std::string conn, Action a);

    private:
        bool is_game_running = true;
        std::unordered_map<std::string, AreaController> areas;
        std::unordered_map<std::string, std::string> playerToArea{};
        std::chrono::steady_clock::time_point last_tick{std::chrono::steady_clock::now()};
        std::list<GameDataWrapper> outgoing{};

        void playerJoinArea(const GameDataWrapper &);

    };
}

#endif //ADVENTURE2019_GAMECONTROLLER_H
