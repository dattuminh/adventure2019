#include "Adventure.h"
#include "Parser.h"

using json = nlohmann::json;
namespace fs = boost::filesystem;

namespace adventure::model {
    /*
     * Read the Area JSON file and deserialize it into the Area model.
     * Return a pair with the Area name as the first value and the model as the second to insert directly into a Map.
     */
    void parseAreaJson(const fs::path &p, std::vector<Area> &areas) {
        Area a;
        try {
            json j;
            fs::ifstream file(p);
            file >> j;
            j.get_to(a);
            areas.push_back(std::move(a));
        } catch (const json::exception &e) {
            LOG(WARNING) << "Ignoring Exception parsing file into JSON at path: " << p << "\n" << e.what();
        }
    }

    Action getAction(const json &j) {
        // TODO: Handle errors
        return model::get_enum<Action>(j, "Action");
    }

    std::vector<Area> loadAreasFromFiles(const fs::path &p) {
        std::vector<Area> areas;
        try {
            for (const auto &file_itr : fs::directory_iterator(p)) {
                LOG(INFO) << "Found file: " << file_itr.path();
                if (file_itr.path().extension() == ".json") {
                    LOG(INFO) << "Parsing file: " << file_itr;
                    parseAreaJson(file_itr, areas);
                }/* else if( file_itr.path().extension() == ".example"){
                    model::parseAreaExampleFile()
                } */
            }
        } catch (const fs::filesystem_error &ex) {
            LOG(ERROR) << "Ignoring Exception from filesystem library in:\n" << ex.what();
        }
        return areas;
    }

    // TODO: Implement or refactor?
    template<typename T>
    inline T getModelFromModelData(const ModelData &d) {
        switch (d.type) {
            case ModelType::Area:break;
            case ModelType::Character:break;
            case ModelType::Door:break;
            case ModelType::Extra:break;
            case ModelType::Inventory:break;
            case ModelType::Item:break;
            case ModelType::NPC:break;
            case ModelType::Object:break;
            case ModelType::Reset:break;
            case ModelType::Room:break;
            case ModelType::Shop:break;
            case ModelType::User:break;
            case ModelType::Nil:break;
        }
        return nullptr;
    }
}
