#include "ValidateInput.h"

namespace adventure::client{

    ValidateInput::ValidateInput(){
        auto comm = CommCommands::_values();
        std::for_each(comm.begin(), comm.end(), [this](CommCommands c){
            commandActionPairs.emplace(c._to_string(), Action::ChatCommand);
        });
        auto inventory = InventoryAction::_values();
        std::for_each(inventory.begin(), inventory.end(), [this](InventoryAction c){
            commandActionPairs.emplace(c._to_string(), Action::Inventory);
        });
        auto look = LookAction::_values();
        std::for_each(look.begin(), look.end(), [this](LookAction c){
            commandActionPairs.emplace(c._to_string(), Action::Look);
        });
        auto debug = DebugCmd::_values();
        std::for_each(debug.begin(), debug.end(), [this](DebugCmd c){
            commandActionPairs.emplace(c._to_string(), Action::Debug);
        });
        auto direction = Direction::_values();
        std::for_each(direction.begin(), direction.end(), [this](Direction c){
            commandActionPairs.emplace(c._to_string(), Action::Move);
        });

        commandActionPairs.emplace("go", Action::Move);
        commandActionPairs.emplace("shutdown", Action::Shutdown);
        commandActionPairs.emplace("heartbeat", Action::Heartbeat);
        commandActionPairs.emplace("status", Action::Status);
        commandActionPairs.emplace("join", Action::Join);
    }

    bool ValidateInput::isValidCommandType(const std::string_view& userInput){
        std::vector<std::string> str;
        boost::split(str, userInput, [](char c) {return c == ' ';});
        boost::to_lower(str.front());
        return commandActionPairs.find(str[0]) != commandActionPairs.end();
    }

    Action ValidateInput::getActionFromString(const std::string_view &commandStr){
        return commandActionPairs.find(commandStr)->second;
    }

    std::string ValidateInput::authInputToJsonString(const std::string &username, const std::string &password) {
        Authenticate auth{.username=username};
        json j = json(auth);
        return j.dump();
    }

    std::string ValidateInput::chatInputToJsonString(const std::string_view &input){
        std::vector<std::string> str;
        boost::split(str, input, [](char c) {return c == ' ';});
        auto com = str[0];
        boost::to_lower(com);
        json j;
        str.erase(str.begin());
        auto arg = boost::join(str, " ");
        j = createJson(com, arg, getActionFromString(com));
        return j.dump();
    }

    json ValidateInput::createJson(const std::string &command, const std::string &args, const Action &action) {
        json j;

        switch(action){
            case Action::Debug:{
                Debug debug{.debugCmd=DebugCmd::_from_string_nocase(command.c_str())};
                j = json(debug);
                break;
            }
            case Action::Join:{
                JoinArea join{.area=args};
                j = json(join);
                break;
            }
            case Action::Move:{
                Move move{.direction=Direction::_from_string_nocase(args.c_str())};
                j = json(move);
                break;
            }
            case Action::ChatCommand:{
                ChatCommand chat{.text=args, .type=CommCommands::_from_string_nocase(command.c_str())};
                j = json(chat);
                break;
            }
            case Action::Look:{
                LookCommand look{.arg=args, .type=LookAction::_from_string_nocase(command.c_str())};
                j = json(look);
                break;
            }
            case Action::Inventory:{
                // TODO: need to differentiate between item/container/character arguments for inventory commands
                InventoryCommand inv{.type=InventoryAction::_from_string_nocase(command.c_str()), .item=args};
                j = json(inv);
                break;
            }
            default:{
                Command com{.sender = "", .command=command, .input=args, .action=action};
                j = json(com);
            }
        }

        return j;
    }

}