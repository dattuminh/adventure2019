#ifndef ADVENTURE2019_AREA_H
#define ADVENTURE2019_AREA_H

#include "Door.h"
#include "Extra.h"
#include "Object.h"
#include "Reset.h"
#include "Room.h"
#include "NPC.h"

#include <vector>
#include <string>

namespace adventure::model {

    class Area {
    public:
        Area() = default;

        Area(std::string n) : name{n} {};

        ~Area() = default;

    private:
        std::string name;
        std::vector<nlohmann::json> helps;
        std::vector<NPC> npcs;
        std::vector<Object> objects;
        std::vector<Room> rooms;
        std::vector<std::vector<Reset>> reset_groups{};
        std::vector<nlohmann::json> shops;
    public:
        const std::string &getName() const { return name; };

        void setName(const std::string &name) { this->name = name; }

        const std::vector<nlohmann::json> &getHelps() const { return helps; }

        void setHelps(const std::vector<nlohmann::json> &value) { this->helps = value; }

        const std::vector<NPC> &getNpcs() const { return npcs; }

        void setNpcs(const std::vector<NPC> &value) { this->npcs = value; }

        const std::vector<Object> &getObjects() const { return objects; }

        void setObjects(const std::vector<Object> &value) { this->objects = value; }

        const std::vector<Room> &getRooms() const { return rooms; }

        void setRooms(const std::vector<Room> &value) { this->rooms = value; }

        const std::vector<std::vector<Reset>> &getResets() const { return reset_groups; }

        void setResets(const std::vector<Reset> &value);

        const std::vector<nlohmann::json> &getShops() const { return shops; }

        void setShops(const std::vector<nlohmann::json> &value) { this->shops = value; }

    };
}

#endif //ADVENTURE2019_AREA_H
