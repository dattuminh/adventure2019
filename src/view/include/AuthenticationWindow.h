#ifndef ADVENTURE2019_LOGINWINDOW_H
#define ADVENTURE2019_LOGINWINDOW_H

#include "Window.h"

#include <functional>
#include <memory>

namespace adventure::view {

    // Note: The ncurses state needs to be hidden from the public interface
    // due to macro conflicts with boost asio. Thus, we hide the implementation
    // requiring state behind indirection for a compilation firewall.
    class AuthWindowImpl;


    class AuthenticationWindow : public Window {
    public:
        explicit AuthenticationWindow(std::function<void(std::string, std::string)> onTextEntry)
                : AuthenticationWindow{std::move(onTextEntry), 1}
        { }

        AuthenticationWindow(std::function<void(std::string, std::string)> onTextEntry, int updateDelay);
        // The default constructor is out of line to defer it until the implementation
        // of AuthWindowImpl is known.
        ~AuthenticationWindow();

        void update() override;

        void redraw() override;

        void displayText(const std::string& text) override;

    private:
        std::unique_ptr<AuthWindowImpl> impl;
    };
}

#endif //ADVENTURE2019_LOGINWINDOW_H
