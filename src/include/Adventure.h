#ifndef ADVENTURE2019_ADVENTURE_H
#define ADVENTURE2019_ADVENTURE_H

#include <nlohmann/json.hpp>
#include <boost/algorithm/string.hpp>
#include <enum.h>
#include <StrongAlias.h>
#include <string>

namespace adventure {
    using json = nlohmann::json;

    BETTER_ENUM(Action, int, ChatCommand, ChatMessage, Move, Heartbeat, Status, Authenticate, Debug, Shutdown, Inventory,
            Join, Look, Register, Model, Error, Confirm, Nil)

    BETTER_ENUM(Direction, int, north, south, east, west, northeast, southeast, northwest, southwest, up, down, Nil)

    BETTER_ENUM(LookAction, int, look, examine);

    BETTER_ENUM(MoveAction, int, go);

    BETTER_ENUM(InventoryAction, int, get, take, put, drop, give, wear, remove);

    BETTER_ENUM(CommCommands, int, says, tell, yell);

    BETTER_ENUM(DebugCmd, int , dump);

    BETTER_ENUM(ModelType, int, Area, Character, Door, Extra, Inventory, Item, NPC, Object, Reset, Room, Shop, User, Nil);

    BETTER_ENUM(SlotLocation, int, light, finger, fingerOther, neck, neckOther, body, head, legs, feet,
                                   hands, arms, shield, about, waist, wrist, wristOther, wield,
                                   hold, dual, ears, eyes, missile, back, face, ankle, ankleOther
    );

    inline void to_json(json &j, const Direction &d) {
        j = json::array({boost::to_lower_copy(std::string{d._to_string()})});
    }

    inline void to_json(json &j, const Action &d) {
        j = json::array({boost::to_lower_copy(std::string{d._to_string()})});
    }

    inline void to_json(json &j, const InventoryAction &d) {
        j = json::array({boost::to_lower_copy(std::string{d._to_string()})});
    }

    inline void to_json(json &j, const CommCommands &c) {
        j = json::array({boost::to_lower_copy(std::string{c._to_string()})});
    }
}

#endif //ADVENTURE2019_ADVENTURE_H
